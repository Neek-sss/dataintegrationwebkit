* [x] Plugin data directories
* [x] Plugin options
* [ ] Separate plugin project from dataintegration project
* [ ] Plugin generator/engine determines compilation/loading based on module kind, which is separate from the plugin kind
* [x] Web plugin index using the loaded plugins (option to not display)
* [x] .dtxz file type for plugin data (is destructively extracted to plugin_data and then deleted)
