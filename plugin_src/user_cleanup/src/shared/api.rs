// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use web_program_core::apis;

apis! {
    "user_cleanup",
    UPLOAD_USERS_TO_CLEANUP ("get") => "upload_users_to_cleanup",
    COMMIT_USERS_TO_CLEANUP ("get") => "commit_users_to_cleanup",
}
