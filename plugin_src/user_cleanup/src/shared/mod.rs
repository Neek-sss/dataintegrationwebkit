// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

pub mod api;

use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Serialize, Deserialize)]
/// The [Account](shared::data::Account)'s that should be checked against existing
/// [Account](shared::data::Account)'s to ensure only the proper [Account](shared::data::Account)'s
/// exist in the datastore
pub struct CheckAccount {
    /// The first name of the [Account](shared::data::Account)
    pub first_name: String,
    /// The last name of the [Account](shared::data::Account)
    pub last_name: String,
    /// The organization that this account falls under
    pub organization: String,
    /// The datastore the [Account](shared::data::Account) is stored in
    pub datastore: String,
}
