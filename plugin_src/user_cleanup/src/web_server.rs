use crate::shared::api::{COMMIT_USERS_TO_CLEANUP, UPLOAD_USERS_TO_CLEANUP};
use crate::shared::CheckAccount;
use log::info;
use plugin_std::macros::export_plugin_func;
use plugin_std::mem::{deserialize, serialize};
use std::collections::{HashMap, HashSet};
use std::error::Error;

#[export_plugin_func("init")]
pub fn init() {
    // Stuff
}

#[export_plugin_func("view")]
pub fn view(page: &str) -> Result<(String, HashMap<String, String>), Box<dyn Error>> {
    match page {
        "" | "index" => {
            let mut context: HashMap<String, String> = HashMap::new();
            context.insert("file_name".to_string(), "user_cleanup".to_string());
            Ok(("wasm".to_string(), context))
        }
        _ => unimplemented!(),
    }
}

#[export_plugin_func("post")]
pub fn post((page, bytes): (String, Vec<u8>)) -> Result<Vec<u8>, Box<dyn Error>> {
    match page.as_str() {
        UPLOAD_USERS_TO_CLEANUP => {
            let cleanup_list = deserialize::<Vec<CheckAccount>>(&bytes);
            info!("Got list: {:?}", cleanup_list);

            let extra_users: HashMap<String, HashMap<String, HashSet<(String, String)>>> =
                HashMap::new();
            let errors: HashMap<(String, String), HashMap<String, HashSet<String>>> =
                HashMap::new();

            Ok(serialize(&(extra_users, errors)))
        }
        COMMIT_USERS_TO_CLEANUP => {
            let cleanup_list = deserialize::<Vec<CheckAccount>>(&bytes);
            info!("Got list: {:?}", cleanup_list);

            Ok(serialize(&()))
        }
        _ => unimplemented!(),
    }
}

#[export_plugin_func("get")]
pub fn get(_page: &str) -> Result<Vec<u8>, Box<dyn Error>> {
    unimplemented!()
}
