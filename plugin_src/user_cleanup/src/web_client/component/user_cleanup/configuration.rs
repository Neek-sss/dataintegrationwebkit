// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use maplit::hashmap;
use std::{collections::HashMap, thread_local};

thread_local! {
    /// The connection between each grade and the organization to place those users in
    pub static GRADE_ORGANIZATIONS: HashMap<String, String> = hashmap!{
        "DK".to_string() => "Students/EL/DK".to_string(),
        "KG".to_string() => "Students/EL/K".to_string(),
        "1".to_string() => "Students/EL/1".to_string(),
        "2".to_string() => "Students/EL/2".to_string(),
        "3".to_string() => "Students/EL/3".to_string(),
        "4".to_string() => "Students/EL/4".to_string(),
        "5".to_string() => "Students/EL/5".to_string(),
        "6".to_string() => "Students/MS/6".to_string(),
        "7".to_string() => "Students/MS/7".to_string(),
        "8".to_string() => "Students/MS/8".to_string(),
        "9".to_string() => "Students/HS/9".to_string(),
        "10".to_string() => "Students/HS/10".to_string(),
        "11".to_string() => "Students/HS/11".to_string(),
        "12".to_string() => "Students/HS/12".to_string(),
        "13".to_string() => "Students/HS/13".to_string(),
    };
}
