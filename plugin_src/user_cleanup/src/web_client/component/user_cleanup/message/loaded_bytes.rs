// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use crate::web_client::component::user_cleanup::{ImportAccount, UserCleanup};
use std::collections::{HashMap, HashSet};
use std::io::{BufReader, Read};
use stringreader::StringReader;
use web_sys::HtmlElement;

/// Process the contents of the chosen file
pub fn update(form: &mut UserCleanup, bytes: Vec<u8>) -> bool {
    form.accounts.clear();
    form.errors.clear();

    let mut data_string = String::new();
    BufReader::new(bytes.as_slice())
        .read_to_string(&mut data_string)
        .expect("Not able to read to string");

    let mut user_vec = Vec::new();
    for user_result in csv::ReaderBuilder::new()
        .from_reader(StringReader::new(&data_string))
        .into_deserialize::<ImportAccount>()
    {
        match user_result {
            Ok(import_account) => {
                user_vec.push(import_account);
            }
            Err(error) => {
                form.errors
                    .entry(("UNKNOWN".to_string(), "UNKNOWN".to_string()))
                    .or_insert_with(HashMap::new)
                    .entry("NONE".to_string())
                    .or_insert_with(HashSet::new)
                    .insert(error.to_string());
            }
        }
    }

    let mut dups = HashSet::new();
    for user in user_vec {
        let name_key = (user.first_name.clone(), user.last_name.clone());
        if dups.contains(&name_key) || form.accounts.contains_key(&name_key) {
            form.accounts.remove(&name_key);
            dups.insert(name_key);
            form.errors
                .entry((user.first_name, user.last_name))
                .or_insert_with(HashMap::default)
                .entry("NONE".to_string())
                .or_insert_with(HashSet::new)
                .insert("Duplicate user".to_string());
        } else if name_key.0.chars().any(|c| !c.is_alphabetic())
            | name_key.1.chars().any(|c| !c.is_alphabetic())
        {
            form.errors
                .entry(name_key.clone())
                .or_insert_with(HashMap::default)
                .entry("NONE".to_string())
                .or_insert_with(HashSet::new)
                .insert("Name contains non-alphabetic characters".to_string());
        } else {
            form.accounts.insert(name_key.clone(), user);
        }
    }

    if form.accounts.is_empty() {
        let upload_button = form.users_ref.cast::<HtmlElement>().unwrap();
        upload_button
            .set_attribute("hidden", "true")
            .expect("Cannot hide users div");
    } else {
        let upload_button = form.users_ref.cast::<HtmlElement>().unwrap();
        upload_button
            .remove_attribute("hidden")
            .expect("Cannot show users div");
    }

    if form.errors.is_empty() {
        let upload_button = form.errors_ref.cast::<HtmlElement>().unwrap();
        upload_button
            .set_attribute("hidden", "true")
            .expect("Cannot hide errors div");
    } else {
        let upload_button = form.errors_ref.cast::<HtmlElement>().unwrap();
        upload_button
            .remove_attribute("hidden")
            .expect("Cannot show errors div");
    }

    true
}
