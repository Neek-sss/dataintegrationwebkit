// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use crate::web_client::component::user_cleanup::UserCleanup;
use gloo::file::File;
use std::collections::{HashMap, HashSet};
use yew::Context;

pub mod commit_user_checks;
pub mod commit_user_checks_fetch_complete;
pub mod file;
pub mod loaded_bytes;
pub mod upload_user_checks;
pub mod upload_user_checks_fetch_complete;

/// Used to process the interactions with the client (both from the server and the user)
pub enum Message {
    LoadedBytes(String, Vec<u8>),
    File(File),
    UploadUserChecks,
    UploadUserChecksFetchComplete(
        (
            HashMap<String, HashMap<String, HashSet<(String, String)>>>,
            HashMap<(String, String), HashMap<String, HashSet<String>>>,
        ),
    ),
    CommitUserChecks,
    CommitUserChecksFetchComplete(HashMap<(String, String), HashMap<String, HashSet<String>>>),
    Other,
}

impl Message {
    /// Process the message data for each message type
    pub fn update(self, form: &mut UserCleanup, ctx: &Context<UserCleanup>) -> bool {
        match self {
            Message::LoadedBytes(_, bytes) => loaded_bytes::update(form, bytes),
            Message::File(file) => file::update(form, ctx, file),
            Message::UploadUserChecks => upload_user_checks::update(form, ctx),
            Message::UploadUserChecksFetchComplete((extra_users, errors)) => {
                upload_user_checks_fetch_complete::update(form, extra_users, errors)
            }
            Message::CommitUserChecks => commit_user_checks::update(form, ctx),
            Message::CommitUserChecksFetchComplete(errors) => {
                commit_user_checks_fetch_complete::update(form, errors)
            }
            Message::Other => false,
        }
    }
}
