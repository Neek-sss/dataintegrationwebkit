// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use crate::shared::api::UPLOAD_USERS_TO_CLEANUP;
use crate::shared::CheckAccount;
use crate::web_client::component::user_cleanup::{
    configuration::GRADE_ORGANIZATIONS, message::Message, UserCleanup,
};
use plugin_std::data::Account;
use std::collections::HashMap;
use web_program_core::shared::web_client::request::web_server_post;
use yew::Context;

/// Send the accounts to the server that should exist
pub fn update(form: &mut UserCleanup, ctx: &Context<UserCleanup>) -> bool {
    if !form.current_upload_fetch_flag {
        form.current_upload_fetch_flag = true;
        let datastores = vec![
            "active_directory_datastore".to_string(),
            "google".to_string(),
            // "sled".to_string(),
        ];
        let changes = form
            .accounts
            .values()
            .flat_map(|import_account| {
                let out_account_option = GRADE_ORGANIZATIONS
                    .try_with(|grade_organizations| {
                        grade_organizations
                            .get(&import_account.grade)
                            .map(|organization| Account {
                                first_name: import_account.first_name.clone(),
                                last_name: import_account.last_name.clone(),
                                user_name: import_account.first_name.clone()
                                    + &import_account.last_name,
                                email: import_account.first_name.clone()
                                    + &import_account.last_name
                                    + "@otsegops.org",
                                organization: organization.clone(),
                                groups: vec![],
                                enabled: true,
                                password: None,
                                password_reset_on_next_login: false,
                            })
                    })
                    .expect("Could not access GRADE_ORGANIZATIONS");

                let mut ret = Vec::new();
                if let Some(out_account) = out_account_option {
                    datastores
                        .iter()
                        .map(move |datastore| CheckAccount {
                            first_name: out_account.first_name.clone(),
                            last_name: out_account.last_name.clone(),
                            organization: out_account.organization.clone(),
                            datastore: datastore.clone(),
                        })
                        .for_each(|edit| ret.push(edit));
                }
                ret.into_iter()
            })
            .collect::<Vec<_>>();

        form.accounts = HashMap::new();

        let link = ctx.link().clone();
        web_server_post(
            &changes,
            UPLOAD_USERS_TO_CLEANUP,
            Box::new(move |data| {
                if let Some(data) = data {
                    link.send_message(Message::UploadUserChecksFetchComplete(data))
                } else {
                    unimplemented!()
                }
            }),
            Box::new(move |_| unimplemented!()),
        );
    }
    false
}
