// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use crate::shared::api::COMMIT_USERS_TO_CLEANUP;
use crate::web_client::component::user_cleanup::{message::Message, UserCleanup};
use web_program_core::shared::web_client::request::web_server_post;
use yew::Context;

/// Sends the accounts to the server for removal
pub fn update(form: &mut UserCleanup, ctx: &Context<UserCleanup>) -> bool {
    if !form.current_commit_fetch_flag {
        form.current_commit_fetch_flag = true;
        let extra_users = form.extra_users.clone();
        form.extra_users.clear();

        let link = ctx.link().clone();
        web_server_post(
            &extra_users,
            COMMIT_USERS_TO_CLEANUP,
            Box::new(move |data| {
                if let Some(data) = data {
                    link.send_message(Message::CommitUserChecksFetchComplete(data))
                } else {
                    unimplemented!()
                }
            }),
            Box::new(move |_| unimplemented!()),
        );
    }
    false
}
