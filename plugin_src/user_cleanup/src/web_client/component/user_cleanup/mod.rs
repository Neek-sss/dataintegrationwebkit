// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use crate::web_client::component::user_cleanup::{html::form_html, message::Message};
use gloo::file::callbacks::FileReader;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::collections::HashSet;
use yew::prelude::*;

pub mod configuration;
pub mod html;
pub mod message;

#[derive(Clone, Debug, Serialize, Deserialize)]
/// The expected formatting for uploaded CSV files to process student [Accounts](Account)
pub struct ImportAccount {
    /// The unique ID for the student
    pub student_id: String,
    /// The first name of the student
    pub first_name: String,
    /// The last name of the student
    pub last_name: String,
    /// The grade that this student is in
    pub grade: String,
}

/// The object representing the data for managing the User Cleanup process
pub struct UserCleanup {
    /// Any files that are having data retrieved from
    pub readers: Vec<FileReader>,
    /// Any errors that need to be displayed to the user
    pub errors: HashMap<(String, String), HashMap<String, HashSet<String>>>,
    /// All of the accounts that have been loaded
    pub accounts: HashMap<(String, String), ImportAccount>,
    /// The extra user accounts to be removed from the domain
    pub extra_users: HashMap<String, HashMap<String, HashSet<(String, String)>>>,
    /// Reference to the users [HTMLElement] display
    pub users_ref: NodeRef,
    /// Reference to the extra_users [HTMLElement] display
    pub extra_users_ref: NodeRef,
    /// Reference to the error [HTMLElement] display
    pub errors_ref: NodeRef,
    pub current_commit_fetch_flag: bool,
    pub current_upload_fetch_flag: bool,
}

impl Component for UserCleanup {
    type Message = Message;
    type Properties = ();

    fn create(_ctx: &Context<Self>) -> Self {
        Self {
            readers: vec![],
            errors: HashMap::new(),
            accounts: HashMap::new(),
            extra_users: HashMap::new(),
            users_ref: NodeRef::default(),
            extra_users_ref: NodeRef::default(),
            errors_ref: NodeRef::default(),
            current_commit_fetch_flag: false,
            current_upload_fetch_flag: false,
        }
    }

    fn update(&mut self, ctx: &Context<Self>, msg: Self::Message) -> bool {
        msg.update(self, ctx)
    }

    fn changed(&mut self, _ctx: &Context<Self>, _props: &Self::Properties) -> bool {
        false
    }

    fn view(&self, ctx: &Context<Self>) -> Html {
        form_html(self, ctx)
    }
}
