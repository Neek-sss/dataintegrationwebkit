// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use crate::web_client::component::user_cleanup::{message::Message, UserCleanup};
use yew::prelude::*;

/// Processes uploading files when the appropriate button is clicked
fn file_upload_selected(_value: Event) -> Message {
    // TODO: Actually do stuff with files
    // if let ChangeData::Files(files) = value {
    //     let file = js_sys::try_iter(&files)
    //         .unwrap()
    //         .unwrap()
    //         .map(|v| web_sys::File::from(v.unwrap()))
    //         .map(File::from)
    //         .last()
    //         .unwrap();
    //     Message::File(file)
    // } else {
    Message::Other
    // }
}

/// The HTML for the User Cleanup form
pub fn form_html(form: &UserCleanup, ctx: &Context<UserCleanup>) -> Html {
    let link = ctx.link().clone();
    html! {
        <div>
            <div>
                <p>{ "Choose a file to upload to user data from" }</p>
                <input type="file" multiple=false onchange={move |_| {link.callback(file_upload_selected);}} />
            </div>
            <div id="users_div" ref={form.users_ref.clone()} hidden=true>
                <p>{ "Users:" }</p>
                <button type="button" onclick={ctx.link().callback(|_| Message::UploadUserChecks)}> {
                    "Upload current users"
                } </button>
                <ul> {
                    for form.accounts.iter().map(|u| html! {
                        <li>{ format!("{u:?}") }</li>
                    })
                } </ul>
            </div>
            <div id="extra_users_div" ref={form.extra_users_ref.clone()} hidden=true>
                <p>{ "Edits:" }</p>
                <button type="button" onclick={ctx.link().callback(|_| Message::CommitUserChecks)}> {
                    "Commit removal of extra users"
                } </button>
                <ul> {
                    for form.extra_users.iter().map(|(organization, datastore_users)| html! {
                        <li>
                            <p>{ organization.clone() }</p>
                            <ul> {
                                for datastore_users.iter().map(|(datastore, names)| html!{
                                    <li>
                                        <p>{ datastore.clone() }</p>
                                        <ul> {
                                            for names.iter().map(|(first_name, last_name)| html!{
                                                <li>{ format!("{first_name} {last_name}") }</li>
                                            })
                                        } </ul>
                                    </li>
                                })
                            } </ul>
                        </li>
                    })
                } </ul>
            </div>
            <div id="errors_div" ref={form.errors_ref.clone()} hidden=true>
                <p>{ "Errors:" }</p>
                <ul>{
                    for form.errors.iter().map(|((first_name, last_name), datastore_list)| html! {
                        <details>
                            <summary>
                                <b>{ format!("{first_name} {last_name}") }</b>
                                { format!(": {}", datastore_list.len()) }
                            </summary>
                            <ul style="list-style-type: none"> {
                                for datastore_list
                                    .iter()
                                    .flat_map(|(datastore, error_list)| error_list
                                        .iter()
                                        .map(move |error| format!("[{datastore}] {error}")))
                                        .map(|s| html! {
                                            <li>
                                                <p> { s } </p>
                                            </li>
                                        })
                            } </ul>
                        </details>
                    })
                }</ul>
            </div>
        </div>
    }
}
