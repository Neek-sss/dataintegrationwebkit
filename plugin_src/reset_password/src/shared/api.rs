// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use web_program_core::apis;

apis! {
    "reset_password",
    RESET_PASSWORD ("post") => "reset_password",
    GENERATE_RESET_PASSWORD_LINK ("post") => "generate_reset_password_link",
    VIEW_RESET_PASSWORD_LINK ("view") => "",
}
