// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use serde::{Deserialize, Serialize};

pub const PASSWORD_RESET_DATA_TYPE: &str = "reset_password/password_reset";

#[derive(Clone, Debug, Serialize, Deserialize)]
pub struct PasswordReset {
    pub id: Vec<u8>,
    pub key: String,
}
