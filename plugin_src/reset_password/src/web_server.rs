// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use crate::shared::api::{GENERATE_RESET_PASSWORD_LINK, RESET_PASSWORD};
use crate::shared::password_reset::{PasswordReset, PASSWORD_RESET_DATA_TYPE};
use datastore_core::shared::datastore_plugin::DatastorePlugin;
use log::{info, warn};
use plugin_std::macros::export_plugin_func;
use plugin_std::mem::{deserialize, serialize};
use rand::distributions::Alphanumeric;
use rand::{thread_rng, Rng};
use std::collections::HashMap;
use std::error::Error;
use std::iter;

#[export_plugin_func("init")]
pub fn init() {
    // Stuff
}

#[export_plugin_func("view")]
pub fn view(key: String) -> Result<(String, HashMap<String, String>), Box<dyn Error>> {
    if let Some(pr) = get_reset_password_link(&key)? {
        let datastore_plugins = DatastorePlugin::all();
        let mut stored_user_opt = None;
        for plugin in &datastore_plugins {
            let stored_user = plugin.get_user(&pr.id)?;
            if stored_user.is_some() {
                stored_user_opt = stored_user;
                break;
            }
        }
        if let Some(user) = stored_user_opt {
            info!("Reset password for {}", user.user_name);

            let mut context: HashMap<String, String> = HashMap::new();
            context.insert("file_name".to_string(), "reset_password".to_string());
            Ok(("wasm".to_string(), context))
        } else {
            let context: HashMap<String, String> = HashMap::new();
            Ok(("password_reset_error".to_string(), context))
        }
    } else {
        let context: HashMap<String, String> = HashMap::new();
        Ok(("password_reset_error".to_string(), context))
    }
}

#[export_plugin_func("post")]
pub fn post((page, bytes): (String, Vec<u8>)) -> Result<Vec<u8>, Box<dyn Error>> {
    match page.as_str() {
        GENERATE_RESET_PASSWORD_LINK => {
            let id = bytes;
            info!("Generating link for ID {:?}", id);

            let password_reset = create_reset_password_link(&id)?;

            Ok(serialize(&password_reset.key))
        }
        RESET_PASSWORD => {
            let (key, password) = deserialize::<(String, String)>(&bytes);
            info!("User key: {}", key);

            if let Some(pr) = get_reset_password_link(&key)? {
                let datastore_plugins = DatastorePlugin::all();
                for plugin in datastore_plugins {
                    plugin.reset_password(&pr.id, &password, false)?;
                }

                delete_reset_password_link(&pr.id)?;
            } else {
                warn!("Invalid password reset link")
            }

            Ok(serialize(&()))
        }
        _ => unimplemented!(),
    }
}

#[export_plugin_func("get")]
pub fn get(_page: String) -> Result<Vec<u8>, Box<dyn Error>> {
    unimplemented!()
}

/// Generates a random password reset link for the user and stores it in the database
pub fn create_reset_password_link(id: &[u8]) -> Result<PasswordReset, Box<dyn Error>> {
    let datastore_plugins = DatastorePlugin::all();
    if let Some(plugin) = datastore_plugins
        .iter()
        .find(|p| p.0.plugin_name == "sled_datastore")
    {
        let password_reset = if let Some(entry) = plugin.get_data::<_, Vec<u8>>(
            "password_reset_name",
            PASSWORD_RESET_DATA_TYPE,
            &id.to_owned(),
        )? {
            deserialize::<PasswordReset>(&entry)
        } else {
            let mut rng = thread_rng();
            let key = iter::repeat(())
                .map(|_| rng.sample(Alphanumeric))
                .map(char::from)
                .take(64)
                .collect::<String>();
            let ret = PasswordReset {
                id: id.to_owned(),
                key: key.clone(),
            };
            plugin.set_data(
                "password_reset_name",
                PASSWORD_RESET_DATA_TYPE,
                &id.to_owned(),
                &ret,
            )?;
            plugin.set_data("password_reset_key", PASSWORD_RESET_DATA_TYPE, &key, &ret)?;
            ret
        };

        info!("Link {:?} generated", password_reset.key);
        Ok(password_reset)
    } else {
        panic!("Cannot get the sled plugin")
    }
}

/// Finds the user data for a given password reset key
fn get_reset_password_link(key: &str) -> Result<Option<PasswordReset>, Box<dyn Error>> {
    let datastore_plugins = DatastorePlugin::all();
    if let Some(plugin) = datastore_plugins
        .iter()
        .find(|p| p.0.plugin_name == "sled_datastore")
    {
        if let Some(password_reset) = plugin.get_data(
            "password_reset_key",
            PASSWORD_RESET_DATA_TYPE,
            &key.to_string(),
        )? {
            Ok(Some(password_reset))
        } else {
            Ok(None)
        }
    } else {
        panic!("Cannot get the sled plugin")
    }
}

/// Removes a password reset link for a given user
fn delete_reset_password_link(id: &[u8]) -> Result<(), Box<dyn Error>> {
    info!("Remove password reset link");
    let datastore_plugins = DatastorePlugin::all();
    if let Some(plugin) = datastore_plugins
        .iter()
        .find(|p| p.0.plugin_name == "sled_datastore")
    {
        plugin.delete_data("password_reset", PASSWORD_RESET_DATA_TYPE, &id.to_owned())?;
    } else {
        panic!("Cannot get the sled plugin")
    }
    Ok(())
}
