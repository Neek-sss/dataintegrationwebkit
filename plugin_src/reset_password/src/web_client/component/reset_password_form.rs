// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use crate::shared::api::RESET_PASSWORD;
use log::{debug, error};
use web_program_core::shared::web_client::request::web_server_post;
use web_sys::{window, InputEvent};
use yew::prelude::*;

pub struct ResetPasswordForm {
    reset_complete: bool,
    password_1: String,
    password_2: String,
    node_ref: NodeRef,
    key: String,
}

/// Used to process the interactions with the client (both from the server and the user)
pub enum Message {
    Password1Input(InputEvent),
    Password2Input(InputEvent),
    Submit,
    Fetch,
    Error,
}

impl Component for ResetPasswordForm {
    type Message = Message;
    type Properties = ();

    fn create(_ctx: &Context<Self>) -> Self {
        let window = window().expect("Cannot open window");
        let document = window.document().expect("Cannot open document");
        let url = document.url().expect("Cannot open url");
        let key = url.split('/').last().expect("Cannot find key");

        debug!("Current key is {}", key);

        Self {
            reset_complete: false,
            password_1: "".to_string(),
            password_2: "".to_string(),
            node_ref: Default::default(),
            key: key.to_string(),
        }
    }

    fn update(&mut self, ctx: &Context<Self>, msg: Self::Message) -> bool {
        match msg {
            Message::Submit => {
                if self.password_1 != self.password_2
                    || self.password_1.is_empty()
                    || self.password_2.is_empty()
                {
                    debug!("Bad password");
                    true
                } else {
                    debug!("Good password");

                    let link_1 = ctx.link().clone();
                    let link_2 = ctx.link().clone();
                    web_server_post(
                        &(self.key.clone(), self.password_1.clone()),
                        RESET_PASSWORD,
                        Box::new(move |data: Option<()>| {
                            if data.is_some() {
                                link_1.send_message(Message::Fetch)
                            } else {
                                link_1.send_message(Message::Error)
                            }
                        }),
                        Box::new(move |_| link_2.send_message(Message::Error)),
                    );

                    false
                }
            }
            Message::Password1Input(input) => {
                self.password_1 = input.data().unwrap();
                debug!("Password 1: {}", self.password_1.clone());
                false
            }
            Message::Password2Input(input) => {
                self.password_2 = input.data().unwrap();
                debug!("Password 2: {}", self.password_2.clone());
                false
            }
            Message::Fetch => {
                debug!("Password successfully reset");
                self.reset_complete = true;
                true
            }
            Message::Error => {
                error!("Error resetting password");
                true
            }
        }
    }

    fn changed(&mut self, _ctx: &Context<Self>, _props: &Self::Properties) -> bool {
        true
    }

    fn view(&self, ctx: &Context<Self>) -> Html {
        let ret = if self.reset_complete {
            html! {
                <p style="position: absolute; top: 35%; left: 40%;">{ "Password Reset!" }</p>
            }
        } else {
            html! {
                <form ref={self.node_ref.clone()} style="position: absolute; top: 35%; left: 40%;">
                    <p>{ "New Passsword:" }</p>
                    <input id="password_1" type="password" oninput={ctx.link().callback(Message::Password1Input)}/>
                    <p>{ "Retype New Passsword:" }</p>
                    <input id="password_2" type="password" oninput={ctx.link().callback(Message::Password2Input)}/>
                    <br/>
                    <input type="button" value="Submit" onclick={ctx.link().callback(|_| Message::Submit)}/>
                </form>
            }
        };
        ret
    }
}
