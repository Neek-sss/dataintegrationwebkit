// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use chrono::Datelike;

use crate::shared::api::UPLOAD_USER_CHANGES;
use crate::web_client::component::import_export_form::{
    configuration::{
        GRADE_GROUPS, GRADE_ORGANIZATIONS, GRADE_PASSWORDS, GRADE_PASSWORD_RESET_ON_LOGIN,
        RESET_PASSWORDS,
    },
    message::Message,
    ImportExportForm,
};
use log::info;
use plugin_std::data::{Account, AccountChange, AccountField};
use std::collections::HashMap;
use web_program_core::shared::web_client::request::web_server_post;
use yew::Context;

/// Transform the user data into edits and send this data to the server to be trimmed down to needed
/// changes only
pub fn update(form: &mut ImportExportForm, ctx: &Context<ImportExportForm>) -> bool {
    if !form.upload_flag {
        form.upload_flag = true;
        let datastores = vec![
            "active_directory_datastore".to_string(),
            // "google_admin_datastore".to_string(),
            "csv_datastore".to_string(),
            "sled_datastore".to_string(),
        ];
        let changes = form
            .users
            .iter()
            .flat_map(|(temp_id, import_account)| {
                let out_account_option = GRADE_ORGANIZATIONS
                    .try_with(|grade_organizations| {
                        grade_organizations
                            .get(&import_account.grade)
                            .map(|organization| Account {
                                first_name: import_account.first_name.clone(),
                                last_name: import_account.last_name.clone(),
                                user_name: import_account.first_name.clone()
                                    + &import_account.last_name,
                                email: import_account.first_name.clone()
                                    + &import_account.last_name
                                    + "@otsegops.org",
                                organization: organization.clone(),
                                groups: vec![],
                                enabled: true,
                                password: None,
                                password_reset_on_next_login: false,
                            })
                    })
                    .expect("Could not access GRADE_ORGANIZATIONS");
                let groups = GRADE_GROUPS
                    .try_with(|grade_groups| grade_groups.get(&import_account.grade).cloned())
                    .expect("Could not access GRADE_GROUPS");
                let reset_password = RESET_PASSWORDS
                    .try_with(|grade_passwords| grade_passwords.get(&import_account.grade).cloned())
                    .expect("Could not access RESET_PASSWORDS");
                let password = GRADE_PASSWORDS
                    .try_with(|grade_passwords| grade_passwords.get(&import_account.grade).cloned())
                    .expect("Could not access GRADE_PASSWORDS");
                let password_reset_on_login = GRADE_PASSWORD_RESET_ON_LOGIN
                    .try_with(|grade_password_reset_on_login| {
                        grade_password_reset_on_login
                            .get(&import_account.grade)
                            .cloned()
                    })
                    .expect("Could not access GRADE_PASSWORDS");
                let mut ret = Vec::new();
                if let Some(mut out_account) = out_account_option {
                    datastores
                        .iter()
                        .flat_map(move |datastore| {
                            let mut ret = vec![
                                // TODO: Make the ID storage work
                                // AccountChange::EditAccount {
                                //     first_name: out_account.first_name.clone(),
                                //     last_name: out_account.last_name.clone(),
                                //     datastore: datastore.clone(),
                                //     change: AccountField::ID(
                                //         out_account.id.clone(),
                                //     ),
                                // },
                                AccountChange::EditAccount {
                                    id: temp_id.clone(),
                                    datastore: datastore.clone(),
                                    change: AccountField::FirstName(out_account.first_name.clone()),
                                },
                                AccountChange::EditAccount {
                                    id: temp_id.clone(),
                                    datastore: datastore.clone(),
                                    change: AccountField::LastName(out_account.last_name.clone()),
                                },
                                AccountChange::EditAccount {
                                    id: temp_id.clone(),
                                    datastore: datastore.clone(),
                                    change: AccountField::UserName(out_account.user_name.clone()),
                                },
                                AccountChange::EditAccount {
                                    id: temp_id.clone(),
                                    datastore: datastore.clone(),
                                    change: AccountField::Email(out_account.email.clone()),
                                },
                                AccountChange::EditAccount {
                                    id: temp_id.clone(),
                                    datastore: datastore.clone(),
                                    change: AccountField::Organization(
                                        out_account.organization.clone(),
                                    ),
                                },
                                AccountChange::EditAccount {
                                    id: temp_id.clone(),
                                    datastore: datastore.clone(),
                                    change: AccountField::Enabled(true),
                                },
                            ];

                            if let Some(password) = &password {
                                let reset_password_on_login =
                                    password_reset_on_login.unwrap_or(false);
                                let edited_password = password.replace(
                                    "{% student_id %}",
                                    &import_account
                                        .student_id
                                        .chars()
                                        .rev()
                                        .take(3)
                                        .collect::<Vec<_>>()
                                        .into_iter()
                                        .rev()
                                        .collect::<String>(),
                                );

                                out_account.password = Some(edited_password.clone());
                                out_account.password_reset_on_next_login = reset_password_on_login;

                                ret.push(AccountChange::AddAccount {
                                    temp_id: temp_id.clone(),
                                    datastore: datastore.clone(),
                                    account: out_account.clone(),
                                });

                                if reset_password.unwrap_or(false) {
                                    ret.push(AccountChange::EditAccount {
                                        id: temp_id.clone(),
                                        datastore: datastore.clone(),
                                        change: AccountField::Password(
                                            edited_password,
                                            reset_password_on_login,
                                        ),
                                    });
                                }
                            }

                            if let Some(groups) = groups.clone() {
                                let date_time = chrono::Local::now();
                                let year = if date_time.month()
                                    > chrono::Month::June.number_from_month()
                                {
                                    date_time.year() + 1
                                } else {
                                    date_time.year()
                                } + (12
                                    - import_account.grade.parse::<i32>().unwrap_or(0));

                                let gs = groups
                                    .into_iter()
                                    .map(|g| g.replace("{% graduation_year %}", &year.to_string()))
                                    .collect::<Vec<_>>();

                                ret.push(AccountChange::EditAccount {
                                    id: temp_id.clone(),
                                    datastore: datastore.clone(),
                                    change: AccountField::Groups(gs),
                                });
                            }

                            ret.into_iter()
                        })
                        .for_each(|edit| ret.push(edit));
                }
                ret.into_iter()
            })
            .collect::<Vec<_>>();

        form.users = HashMap::new();

        let link = ctx.link().clone();
        info!("{:?}", plugin_std::mem::serialize(&changes));
        web_server_post(
            &changes,
            UPLOAD_USER_CHANGES,
            Box::new(move |data| {
                if let Some(data) = data {
                    link.send_message(Message::UploadUserEditsFetchComplete(data))
                } else {
                    unimplemented!()
                }
            }),
            Box::new(move |_| unimplemented!()),
        );
    }
    false
}
