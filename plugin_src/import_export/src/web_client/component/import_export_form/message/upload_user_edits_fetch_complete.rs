// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use crate::web_client::component::import_export_form::ImportExportForm;
use plugin_std::data::AccountChange;
use std::collections::{HashMap, HashSet};
use web_sys::HtmlElement;

/// Read the server response to the uploaded user edits
pub fn update(
    form: &mut ImportExportForm,
    edits: Vec<AccountChange>,
    errors: HashMap<Vec<u8>, HashMap<String, HashSet<String>>>,
) -> bool {
    form.edits.clear();
    for edit in edits {
        let id = match &edit {
            AccountChange::AddAccount { temp_id, .. } => temp_id.clone(),
            AccountChange::EditAccount { id, .. } => id.clone(),
            AccountChange::RemoveAccount { .. } => unimplemented!(),
        };
        form.edits
            .entry(id)
            .or_insert_with(Vec::new)
            .push(edit.clone());
        form.out_edits.insert(edit);
    }

    for (name, datastore_list) in errors {
        for (datastore, error_list) in datastore_list {
            for error in error_list {
                form.errors
                    .entry(name.clone())
                    .or_insert_with(HashMap::new)
                    .entry(datastore.clone())
                    .or_insert_with(HashSet::new)
                    .insert(error);
            }
        }
    }

    if form.users.is_empty() {
        let upload_button = form.users_ref.cast::<HtmlElement>().unwrap();
        upload_button
            .set_attribute("hidden", "true")
            .expect("Cannot hide users div");
    } else {
        let upload_button = form.users_ref.cast::<HtmlElement>().unwrap();
        upload_button
            .remove_attribute("hidden")
            .expect("Cannot show users div");
    }

    if form.edits.is_empty() {
        let upload_button = form.edits_ref.cast::<HtmlElement>().unwrap();
        upload_button
            .set_attribute("hidden", "true")
            .expect("Cannot hide edits div");
    } else {
        let upload_button = form.edits_ref.cast::<HtmlElement>().unwrap();
        upload_button
            .remove_attribute("hidden")
            .expect("Cannot show edits div");
    }

    if form.errors.is_empty() {
        let upload_button = form.errors_ref.cast::<HtmlElement>().unwrap();
        upload_button
            .set_attribute("hidden", "true")
            .expect("Cannot hide errors div");
    } else {
        let upload_button = form.errors_ref.cast::<HtmlElement>().unwrap();
        upload_button
            .remove_attribute("hidden")
            .expect("Cannot show errors div");
    }

    form.upload_flag = false;
    true
}
