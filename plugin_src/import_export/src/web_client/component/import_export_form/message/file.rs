// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use crate::web_client::component::import_export_form::{message::Message, ImportExportForm};
use gloo::file::File;
use yew::Context;

/// Read the chosen file
pub fn update(form: &mut ImportExportForm, ctx: &Context<ImportExportForm>, file: File) -> bool {
    let task = {
        let file_name = file.name();
        let link = ctx.link().clone();

        gloo::file::callbacks::read_as_bytes(&file, move |res| {
            link.send_message(Message::LoadedBytes(
                file_name,
                res.expect("failed to read file"),
            ))
        })
    };
    form.readers.push(task);
    true
}
