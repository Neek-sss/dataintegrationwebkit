// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use crate::shared::api::COMMIT_USER_CHANGES;
use crate::web_client::component::import_export_form::{message::Message, ImportExportForm};
use web_program_core::shared::web_client::request::web_server_post;
use yew::prelude::*;

/// Send the user edit data to the server to be processed
pub fn update(form: &mut ImportExportForm, ctx: &Context<ImportExportForm>) -> bool {
    if !form.commit_flag {
        form.commit_flag = true;
        let edits = form.out_edits.clone();
        form.edits.clear();

        let link = ctx.link().clone();
        web_server_post(
            &edits,
            COMMIT_USER_CHANGES,
            Box::new(move |data| {
                if let Some(data) = data {
                    link.send_message(Message::CommitUserEditsFetchComplete(data))
                } else {
                    unimplemented!()
                }
            }),
            Box::new(move |_| unimplemented!()),
        );
    }
    false
}
