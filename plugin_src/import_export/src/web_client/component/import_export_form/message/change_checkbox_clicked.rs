// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use crate::web_client::component::import_export_form::ImportExportForm;
use plugin_std::data::AccountChange;
use web_sys::HtmlInputElement;
use yew::NodeRef;

/// Process the checkbox click
pub fn update(
    form: &mut ImportExportForm,
    input_ref: NodeRef,
    account_change: AccountChange,
) -> bool {
    let input = input_ref.cast::<HtmlInputElement>().unwrap();

    if input.checked() {
        form.out_edits.insert(account_change.clone());
    } else {
        form.out_edits.remove(&account_change);
    }

    false
}
