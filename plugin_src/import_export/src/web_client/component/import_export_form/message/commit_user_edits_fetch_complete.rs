// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use crate::web_client::component::import_export_form::ImportExportForm;
use std::collections::{HashMap, HashSet};
use web_sys::HtmlElement;

/// Process the server response the the committed user data
pub fn update(
    form: &mut ImportExportForm,
    errors: HashMap<Vec<u8>, HashMap<String, HashSet<String>>>,
) -> bool {
    for (id, datastore_list) in errors {
        for (datastore, error_list) in datastore_list {
            for error in error_list {
                form.errors
                    .entry(id.clone())
                    .or_insert_with(HashMap::new)
                    .entry(datastore.clone())
                    .or_insert_with(HashSet::new)
                    .insert(error);
            }
        }
    }

    form.commit_flag = false;

    if form.users.is_empty() {
        let upload_button = form.users_ref.cast::<HtmlElement>().unwrap();
        upload_button
            .set_attribute("hidden", "true")
            .expect("Cannot hide users div");
    } else {
        let upload_button = form.users_ref.cast::<HtmlElement>().unwrap();
        upload_button
            .remove_attribute("hidden")
            .expect("Cannot show users div");
    }

    if form.edits.is_empty() {
        let upload_button = form.edits_ref.cast::<HtmlElement>().unwrap();
        upload_button
            .set_attribute("hidden", "true")
            .expect("Cannot hide edits div");
    } else {
        let upload_button = form.edits_ref.cast::<HtmlElement>().unwrap();
        upload_button
            .remove_attribute("hidden")
            .expect("Cannot show edits div");
    }

    if form.errors.is_empty() {
        let upload_button = form.errors_ref.cast::<HtmlElement>().unwrap();
        upload_button
            .set_attribute("hidden", "true")
            .expect("Cannot hide errors div");
    } else {
        let upload_button = form.errors_ref.cast::<HtmlElement>().unwrap();
        upload_button
            .remove_attribute("hidden")
            .expect("Cannot show errors div");
    }

    true
}
