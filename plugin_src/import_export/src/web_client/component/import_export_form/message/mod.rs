// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use crate::web_client::component::import_export_form::ImportExportForm;
use gloo::file::File;
use plugin_std::data::AccountChange;
use std::collections::{HashMap, HashSet};
use yew::prelude::*;

mod change_checkbox_clicked;
mod commit_user_edits;
mod commit_user_edits_fetch_complete;
mod file;
mod loaded_bytes;
mod upload_user_edits;
mod upload_user_edits_fetch_complete;

/// Used to process the interactions with the client (both from the server and the user)
pub enum Message {
    /// Bytes have been loaded from the file
    LoadedBytes(String, Vec<u8>),
    /// The file has been loaded
    File(File),
    /// User edits should be uploaded to be checked
    UploadUserEdits,
    /// The server has responded to the user edit upload
    UploadUserEditsFetchComplete(
        (
            Vec<AccountChange>,
            HashMap<Vec<u8>, HashMap<String, HashSet<String>>>,
        ),
    ),
    /// Any time that the checkbox has been checked indicating a change in the status of whether an
    /// account should be changed on upload
    ChangeCheckboxClicked(NodeRef, AccountChange),
    /// The user edits should be commited to the server, actually performing the edits
    CommitUserEdits,
    /// The server responded to the user edit commit
    CommitUserEditsFetchComplete(HashMap<Vec<u8>, HashMap<String, HashSet<String>>>),
    /// Anything else that might be messaged
    Other,
}

impl Message {
    /// Process the message data for each message type
    pub fn update(self, form: &mut ImportExportForm, ctx: &Context<ImportExportForm>) -> bool {
        match self {
            Message::LoadedBytes(_, bytes) => loaded_bytes::update(form, bytes),
            Message::File(file) => file::update(form, ctx, file),
            Message::UploadUserEdits => upload_user_edits::update(form, ctx),
            Message::UploadUserEditsFetchComplete((edits, errors)) => {
                upload_user_edits_fetch_complete::update(form, edits, errors)
            }
            Message::ChangeCheckboxClicked(input_ref, account_change) => {
                change_checkbox_clicked::update(form, input_ref, account_change)
            }
            Message::CommitUserEdits => commit_user_edits::update(form, ctx),
            Message::CommitUserEditsFetchComplete(errors) => {
                commit_user_edits_fetch_complete::update(form, errors)
            }
            Message::Other => false,
        }
    }
}
