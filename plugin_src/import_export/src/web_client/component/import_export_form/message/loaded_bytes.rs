// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use crate::web_client::component::import_export_form::{ImportAccount, ImportExportForm};
use plugin_std::mem::serialize;
use std::collections::{HashMap, HashSet};
use std::io::{BufReader, Read};
use stringreader::StringReader;
use web_sys::HtmlElement;

/// Process the contents of the chosen file
pub fn update(form: &mut ImportExportForm, bytes: Vec<u8>) -> bool {
    form.users.clear();
    form.errors.clear();

    let mut data_string = String::new();
    BufReader::new(bytes.as_slice())
        .read_to_string(&mut data_string)
        .expect("Not able to read to string");

    let mut user_vec = Vec::new();
    for user_result in csv::ReaderBuilder::new()
        .from_reader(StringReader::new(&data_string))
        .into_deserialize::<ImportAccount>()
    {
        match user_result {
            Ok(import_account) => {
                user_vec.push(import_account);
            }
            Err(error) => {
                form.errors
                    .entry(vec![])
                    .or_insert_with(HashMap::new)
                    .entry("NONE".to_string())
                    .or_insert_with(HashSet::new)
                    .insert(error.to_string());
            }
        }
    }

    let mut dups = HashSet::new();
    for user in user_vec {
        let student_id_key = serialize(&user.student_id);
        if dups.contains(&student_id_key) || form.users.contains_key(&student_id_key) {
            form.users.remove(&student_id_key);
            dups.insert(student_id_key.clone());
            form.errors
                .entry(student_id_key.clone())
                .or_insert_with(HashMap::default)
                .entry("NONE".to_string())
                .or_insert_with(HashSet::new)
                .insert("Duplicate user".to_string());
        } else if user.first_name.chars().any(|c| !c.is_alphabetic())
            | user.last_name.chars().any(|c| !c.is_alphabetic())
        {
            form.errors
                .entry(student_id_key.clone())
                .or_insert_with(HashMap::default)
                .entry("NONE".to_string())
                .or_insert_with(HashSet::new)
                .insert("Name contains non-alphabetic characters".to_string());
        } else {
            form.users.insert(student_id_key, user);
        }
    }

    if form.users.is_empty() {
        let upload_button = form.users_ref.cast::<HtmlElement>().unwrap();
        upload_button
            .set_attribute("hidden", "true")
            .expect("Cannot hide users div");
    } else {
        let upload_button = form.users_ref.cast::<HtmlElement>().unwrap();
        upload_button
            .remove_attribute("hidden")
            .expect("Cannot show users div");
    }

    if form.errors.is_empty() {
        let upload_button = form.errors_ref.cast::<HtmlElement>().unwrap();
        upload_button
            .set_attribute("hidden", "true")
            .expect("Cannot hide errors div");
    } else {
        let upload_button = form.errors_ref.cast::<HtmlElement>().unwrap();
        upload_button
            .remove_attribute("hidden")
            .expect("Cannot show errors div");
    }

    true
}
