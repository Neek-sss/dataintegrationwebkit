// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use gloo::file::callbacks::FileReader;
use plugin_std::data::AccountChange;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use std::collections::HashSet;
use yew::prelude::*;

pub mod configuration;
pub mod html;
pub mod message;

use crate::web_client::component::import_export_form::{html::form_html, message::Message};

#[derive(Clone, Debug, Serialize, Deserialize)]
/// The expected formatting for uploaded CSV files to process student [Accounts](Account)
pub struct ImportAccount {
    /// The unique ID for the student
    pub student_id: String,
    /// The first name of the student
    pub first_name: String,
    /// The last name of the student
    pub last_name: String,
    /// The grade that this student is in
    pub grade: String,
}

/// The actual form that the page will display, along with all interactions
pub struct ImportExportForm {
    /// Any files that are having data retrieved from
    pub readers: Vec<FileReader>,
    /// Any errors that need to be displayed to the user
    pub errors: HashMap<Vec<u8>, HashMap<String, HashSet<String>>>,
    /// All of the users that have been loaded
    pub users: HashMap<Vec<u8>, ImportAccount>,
    /// All edits that need to be made to the current accounts
    pub edits: HashMap<Vec<u8>, Vec<AccountChange>>,
    /// The edits that are actually sent to the server (stripped of the extra name organization)
    pub out_edits: HashSet<AccountChange>,
    pub input_file_ref: NodeRef,
    /// Reference to the user [HTMLElement] display
    pub users_ref: NodeRef,
    /// Reference to the edit [HTMLElement] display
    pub edits_ref: NodeRef,
    /// Reference to the error [HTMLElement] display
    pub errors_ref: NodeRef,
    pub upload_flag: bool,
    pub commit_flag: bool,
}

impl Component for ImportExportForm {
    type Message = Message;
    type Properties = ();

    fn create(_ctx: &Context<Self>) -> Self {
        Self {
            readers: vec![],
            users: HashMap::new(),
            edits: HashMap::new(),
            out_edits: HashSet::new(),
            errors: HashMap::new(),
            input_file_ref: NodeRef::default(),
            users_ref: NodeRef::default(),
            edits_ref: NodeRef::default(),
            errors_ref: NodeRef::default(),
            upload_flag: false,
            commit_flag: false,
        }
    }

    fn update(&mut self, ctx: &Context<Self>, msg: Self::Message) -> bool {
        msg.update(self, ctx)
    }

    fn changed(&mut self, _ctx: &Context<Self>, _props: &Self::Properties) -> bool {
        false
    }

    fn view(&self, ctx: &Context<Self>) -> Html {
        form_html(self, ctx)
    }
}
