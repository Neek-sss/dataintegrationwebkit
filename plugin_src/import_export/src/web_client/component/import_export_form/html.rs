// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use crate::web_client::component::import_export_form::{message::Message, ImportExportForm};
use gloo::file::File;
use log::info;
use web_sys::HtmlInputElement;
use yew::prelude::*;

/// Processes uploading files when the appropriate button is clicked
fn file_upload_selected(input_ref: NodeRef) -> Message {
    let input = input_ref.cast::<HtmlInputElement>().unwrap();
    // TODO: Files
    if let Some(file) = input.files().and_then(|fl| fl.get(0)) {
        info!("File: {}", file.name());
        Message::File(File::from(file))
    } else {
        Message::Other
    }
}

/// The HTML for the Import/Export Form
pub fn form_html(form: &ImportExportForm, ctx: &Context<ImportExportForm>) -> Html {
    let link = ctx.link().clone();
    let input_ref = form.input_file_ref.clone();
    html! {
        <div>
            <div>
                <p>{ "Choose a file to upload to user data from" }</p>
                <input
                    type="file"
                    multiple=false
                    ref={form.input_file_ref.clone()}
                    onchange={link.callback(move |_| file_upload_selected(input_ref.clone()))}
                />
            </div>
            <div id="users_div" ref={form.users_ref.clone()} hidden=true>
                <p>{ "Users:" }</p>
                <button type="button" onclick={ctx.link().callback(|_| Message::UploadUserEdits)}> {
                    "Upload user changes"
                } </button>
                <ul> {
                    for form.users.iter().map(|u| html! {
                        <li>{ format!("{u:?}") }</li>
                    })
                } </ul>
            </div>
            <div id="edits_div" ref={form.edits_ref.clone()} hidden=true>
                <p>{ "Edits:" }</p>
                <button type="button" onclick={ctx.link().callback(|_| Message::CommitUserEdits)}> {
                    "Commit user changes"
                } </button>
                <ul> {
                    for form.edits.iter().map(|(u, cv)| html! {
                        <details>
                            <summary>
                                <b>{ format!("{u:?}") }</b>
                                { format!(": {}", cv.len()) }
                            </summary>
                            <ul style="list-style-type: none"> {
                                for cv.iter().cloned().enumerate().map(|(i, c)| {
                                    let id = format!("{u:?}{i}");
                                    let label = format!("{c}");
                                    let account = c.clone();
                                    let input_ref = NodeRef::default();
                                    (id, label, account, input_ref)
                                }).map(|(id, label, account, input_ref)| html! {
                                    <li>
                                        <input
                                            type="checkbox"
                                            name={ id.clone() }
                                            value={ id.clone() }
                                            ref={input_ref.clone()}
                                            onclick={ctx.link().callback(move |_| Message::ChangeCheckboxClicked(input_ref.clone(), account.clone()))}
                                            checked=true
                                        />
                                        <label for={ id.clone() }> { label } </label>
                                    </li>
                                })
                            } </ul>
                        </details>
                    })
                } </ul>
            </div>
            <div id="errors_div" ref={form.errors_ref.clone()} hidden=true>
                <p>{ "Errors:" }</p>
                <ul>{
                    for form.errors.iter().map(|(id, datastore_list)| html! {
                        <details>
                            <summary>
                                <b>{ format!("{id:?}") }</b>
                                { format!(": {}", datastore_list.len()) }
                            </summary>
                            <ul style="list-style-type: none"> {
                                for datastore_list
                                    .iter()
                                    .flat_map(|(datastore, error_list)| error_list
                                        .iter()
                                        .map(move |error| format!("[{datastore}] {error}")))
                                        .map(|s| html! {
                                            <li>
                                                <p> { s } </p>
                                            </li>
                                        })
                            } </ul>
                        </details>
                    })
                }</ul>
            </div>
        </div>
    }
}
