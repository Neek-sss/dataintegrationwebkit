// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use maplit::hashmap;
use std::{collections::HashMap, thread_local};

thread_local! {
    /// The connection between each grade and the organization to place those users in
    pub static GRADE_ORGANIZATIONS: HashMap<String, String> = hashmap!{
        "DK".to_string() => "Students/EL/DK".to_string(),
        "KG".to_string() => "Students/EL/K".to_string(),
        "1".to_string() => "Students/EL/1".to_string(),
        "2".to_string() => "Students/EL/2".to_string(),
        "3".to_string() => "Students/EL/3".to_string(),
        "4".to_string() => "Students/EL/4".to_string(),
        "5".to_string() => "Students/EL/5".to_string(),
        "6".to_string() => "Students/MS/6".to_string(),
        "7".to_string() => "Students/MS/7".to_string(),
        "8".to_string() => "Students/MS/8".to_string(),
        "9".to_string() => "Students/HS/9".to_string(),
        "10".to_string() => "Students/HS/10".to_string(),
        "11".to_string() => "Students/HS/11".to_string(),
        "12".to_string() => "Students/HS/12".to_string(),
        "13".to_string() => "Students/HS/13".to_string(),
    };

    /// How to set the passwords for each grade
    pub static GRADE_PASSWORDS: HashMap<String, String> = hashmap!{
        "DK".to_string() => "Bulldogs".to_string(),
        "KG".to_string() => "Bulldogs".to_string(),
        "1".to_string() => "Bulldogs".to_string(),
        "2".to_string() => "Bulldogs".to_string(),
        "3".to_string() => "Bulldogs{% student_id %}".to_string(),
        "4".to_string() => "Bulldogs{% student_id %}".to_string(),
        "5".to_string() => "Bulldogs{% student_id %}".to_string(),
        "6".to_string() => "Bulldogs{% student_id %}".to_string(),
        "7".to_string() => "Bulldogs{% student_id %}".to_string(),
        "8".to_string() => "Bulldogs{% student_id %}".to_string(),
        "9".to_string() => "Bulldogs{% student_id %}".to_string(),
        "10".to_string() => "Bulldogs{% student_id %}".to_string(),
        "11".to_string() => "Bulldogs{% student_id %}".to_string(),
        "12".to_string() => "Bulldogs{% student_id %}".to_string(),
        "13".to_string() => "Bulldogs{% student_id %}".to_string(),
    };

    /// Whether each grade should be allowed to reset their passwords
    pub static GRADE_PASSWORD_RESET_ON_LOGIN: HashMap<String, bool> = hashmap!{
        "DK".to_string() => false,
        "KG".to_string() => false,
        "1".to_string() => false,
        "2".to_string() => false,
        "3".to_string() => false,
        "4".to_string() => false,
        "5".to_string() => false,
        "6".to_string() => true,
        "7".to_string() => true,
        "8".to_string() => true,
        "9".to_string() => true,
        "10".to_string() => true,
        "11".to_string() => true,
        "12".to_string() => true,
        "13".to_string() => true,
    };

    /// Which groups each student should be assigned to based on their grades
    pub static GRADE_GROUPS: HashMap<String, Vec<String>> = hashmap!{
        "DK".to_string() => vec!["Class of {% graduation_year %}".to_string()],
        "KG".to_string() => vec!["Class of {% graduation_year %}".to_string()],
        "1".to_string() => vec!["Class of {% graduation_year %}".to_string()],
        "2".to_string() => vec!["Class of {% graduation_year %}".to_string()],
        "3".to_string() => vec!["Class of {% graduation_year %}".to_string()],
        "4".to_string() => vec!["Class of {% graduation_year %}".to_string()],
        "5".to_string() => vec!["Class of {% graduation_year %}".to_string()],
        "6".to_string() => vec!["Class of {% graduation_year %}".to_string(), "MS_Students".to_string()],
        "7".to_string() => vec!["Class of {% graduation_year %}".to_string(), "MS_Students".to_string()],
        "8".to_string() => vec!["Class of {% graduation_year %}".to_string(), "MS_Students".to_string()],
        "9".to_string() => vec!["Class of {% graduation_year %}".to_string(), "HS_Students".to_string()],
        "10".to_string() => vec!["Class of {% graduation_year %}".to_string(), "HS_Students".to_string()],
        "11".to_string() => vec!["Class of {% graduation_year %}".to_string(), "HS_Students".to_string()],
        "12".to_string() => vec!["Class of {% graduation_year %}".to_string(), "HS_Students".to_string()],
        "13".to_string() => vec!["Class of {% graduation_year %}".to_string(), "HS_Students".to_string()],
    };

    /// Whether or not to reset the passwords for each student based on the grades
    pub static RESET_PASSWORDS: HashMap<String, bool> = hashmap! {
        "DK".to_string() => true,
        "KG".to_string() => true,
        "1".to_string() => true,
        "2".to_string() => true,
        "3".to_string() => true,
        "4".to_string() => true,
        "5".to_string() => true,
        "6".to_string() => true,
        "7".to_string() => false,
        "8".to_string() => false,
        "9".to_string() => false,
        "10".to_string() => false,
        "11".to_string() => false,
        "12".to_string() => false,
        "13".to_string() => false,
    }
}
