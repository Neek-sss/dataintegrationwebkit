// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

#![recursion_limit = "512"]

pub mod shared;

#[cfg(feature = "web_client")]
pub mod web_client;

#[cfg(feature = "web_server")]
pub mod web_server;
