// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use web_program_core::apis;

apis! {
    "import_export",
    UPLOAD_USER_CHANGES ("post") => "upload_user_changes",
    COMMIT_USER_CHANGES ("post") => "commit_user_changes",
}
