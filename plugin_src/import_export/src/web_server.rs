// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use crate::shared::api::{COMMIT_USER_CHANGES, UPLOAD_USER_CHANGES};
use log::{debug, error, info};
use plugin_std::data::{AccountChange, AccountField};
use plugin_std::macros::export_plugin_func;
use std::collections::{HashMap, HashSet};
use std::error::Error;
use std::time::Duration;

#[export_plugin_func("init")]
pub fn init() {
    // Stuff
}

#[export_plugin_func("view")]
pub fn view(page: &str) -> Result<(String, HashMap<String, String>), Box<dyn Error>> {
    match page {
        "" | "index" => {
            let mut context: HashMap<String, String> = HashMap::new();
            context.insert("file_name".to_string(), "import_export".to_string());
            Ok(("wasm".to_string(), context))
        }
        _ => unimplemented!(),
    }
}

use datastore_core::shared::datastore_plugin::DatastorePlugin;
use plugin_shared::{deserialize, serialize};
use serde::{Deserialize, Serialize};

#[derive(Clone, Debug, Serialize, Deserialize)]
struct PasswordRecord {
    datastore: String,
    username: String,
    password: String,
}

#[export_plugin_func("post")]
pub fn post((page, bytes): (String, Vec<u8>)) -> Result<Vec<u8>, Box<dyn Error>> {
    let datastore_plugins = DatastorePlugin::all();
    match page.as_str() {
        UPLOAD_USER_CHANGES => {
            let mut changes = deserialize::<Vec<AccountChange>>(&bytes);

            changes.sort();

            let length = changes.len();

            info!("Got {} user changes to make", length);

            let mut error_strings = HashMap::new();

            let mut id_maps = HashMap::new();

            let mut last_percent = 0;
            changes = changes
                .into_iter()
                .enumerate()
                .filter(|(i, change)| {
                    let mut current_percent =
                        ((*i as f64 / length as f64) * 100.0).round() as usize;
                    if current_percent == 100 {
                        current_percent = 99;
                    }
                    if current_percent != last_percent {
                        info!("{}% complete", current_percent);
                        last_percent = current_percent;
                    }
                    debug!("Change: {:?}", change);

                    let mut f = || -> Result<bool, Box<dyn Error>> {
                        Ok(match change {
                            AccountChange::AddAccount {
                                datastore,
                                account,
                                temp_id,
                                ..
                            } => {
                                info!("Add: {:?} {} {:?}", temp_id, datastore, account);
                                if let Some(plugin) = datastore_plugins
                                    .iter()
                                    .find(|p| &p.0.plugin_name == datastore)
                                {
                                    let account_vec = plugin.search_user(&format!(
                                        "{},{}",
                                        account.first_name, account.last_name
                                    ))?;
                                    if let Some((id, _)) = account_vec.first() {
                                        info!("Existing User ID: {:?}", id);
                                        id_maps.insert(
                                            (datastore.clone(), temp_id.clone()),
                                            (datastore.clone(), id.clone()),
                                        );
                                    }
                                    account_vec.is_empty()
                                } else {
                                    false
                                }
                            }
                            AccountChange::EditAccount {
                                id,
                                datastore,
                                change,
                            } => {
                                info!("Edit: {:?} {} {:?}", id, datastore, change);
                                let mut id = id.clone();
                                if let Some((_, new_id)) =
                                    id_maps.get(&(datastore.clone(), id.clone()))
                                {
                                    id = new_id.clone();
                                }

                                if let Some(account) = if let Some(plugin) = datastore_plugins
                                    .iter()
                                    .find(|p| &p.0.plugin_name == datastore)
                                {
                                    let x = plugin.get_user(&id)?;
                                    info!("Existing User: {:?}", x);
                                    x
                                } else {
                                    None
                                } {
                                    match change {
                                        AccountField::ID(value) => &id != value,
                                        AccountField::FirstName(value) => {
                                            &account.first_name != value
                                        }
                                        AccountField::LastName(value) => {
                                            &account.last_name != value
                                        }
                                        AccountField::UserName(value) => {
                                            account.user_name.to_ascii_lowercase()
                                                != value.to_ascii_lowercase()
                                        }
                                        AccountField::Email(value) => {
                                            account.email.to_ascii_lowercase()
                                                != value.to_ascii_lowercase()
                                        }
                                        AccountField::Organization(value) => {
                                            &account.organization != value
                                        }
                                        AccountField::Password(_, _) => true,
                                        AccountField::Groups(value) => {
                                            !account.groups.iter().all(|g| value.contains(g))
                                                || !value.iter().all(|g| account.groups.contains(g))
                                        }
                                        AccountField::Enabled(value) => account.enabled != *value,
                                    }
                                } else {
                                    false
                                }
                            }
                            AccountChange::RemoveAccount { .. } => false,
                        })
                    };
                    match f() {
                        Ok(ret) => ret,
                        Err(e) => {
                            error!("{}", e);
                            let (id, datastore) = match change {
                                AccountChange::AddAccount {
                                    datastore, temp_id, ..
                                } => (temp_id.clone(), datastore.clone()),
                                AccountChange::RemoveAccount { datastore, id } => {
                                    (id.clone(), datastore.clone())
                                }
                                AccountChange::EditAccount { id, datastore, .. } => {
                                    (id.clone(), datastore.clone())
                                }
                            };
                            error_strings
                                .entry(id)
                                .or_insert_with(HashMap::new)
                                .entry(datastore)
                                .or_insert_with(HashSet::new)
                                .insert(e.to_string());
                            false
                        }
                    }
                })
                .map(|(_, change)| change)
                .collect();

            for change in changes.iter_mut() {
                match change {
                    AccountChange::AddAccount { .. } => {}
                    AccountChange::EditAccount { id, datastore, .. }
                    | AccountChange::RemoveAccount { id, datastore, .. } => {
                        if let Some(new_id) = id_maps.get(&(datastore.clone(), id.clone())) {
                            *id = new_id.1.clone();
                        }
                    }
                }
            }

            info!("Only need to make {} user changes", changes.len());

            Ok(serialize(&(changes, error_strings)))
        }
        COMMIT_USER_CHANGES => {
            let changes = deserialize::<Vec<AccountChange>>(&bytes);

            let length = changes.len();

            info!("Got {} user changes to make", length);

            let mut error_strings = HashMap::new();

            let mut last_percent = 0;
            for (i, change) in changes.into_iter().enumerate() {
                let mut current_percent = ((i as f64 / length as f64) * 100.0).round() as usize;
                if current_percent == 100 {
                    current_percent = 99;
                }
                if current_percent != last_percent {
                    info!("{}% complete", current_percent);
                    last_percent = current_percent;
                }
                debug!("Change: {:?}", change);

                let f = || -> Result<(), Box<dyn Error>> {
                    match change.clone() {
                        AccountChange::AddAccount {
                            datastore, account, ..
                        } => {
                            info!(
                                "Adding {} {} to {}",
                                account.first_name, account.last_name, datastore
                            );
                            if let Some(plugin) = datastore_plugins
                                .iter()
                                .find(|p| p.0.plugin_name == datastore)
                            {
                                plugin.add_user(&account)?;
                            }
                        }
                        AccountChange::EditAccount {
                            id,
                            datastore,
                            change,
                        } => {
                            info!("Editing {:?} in {} ({:?})", id, datastore, change);
                            if let Some(mut account) = if let Some(plugin) = datastore_plugins
                                .iter()
                                .find(|p| p.0.plugin_name == datastore)
                            {
                                plugin.get_user(&id)?
                            } else {
                                None
                            } {
                                info!("Process account change: {:?}", change);
                                match change.clone() {
                                    AccountField::FirstName(value) => account.first_name = value,
                                    AccountField::LastName(value) => account.last_name = value,
                                    AccountField::UserName(value) => account.user_name = value,
                                    AccountField::Email(value) => account.email = value,
                                    AccountField::Organization(value) => {
                                        account.organization = value
                                    }
                                    AccountField::Groups(value) => account.groups = value,
                                    AccountField::Enabled(value) => account.enabled = value,
                                    _ => {}
                                }
                                if let AccountField::Password(password, password_reset_on_login) =
                                    change
                                {
                                    if let Some(plugin) = datastore_plugins
                                        .iter()
                                        .find(|p| p.0.plugin_name == datastore)
                                    {
                                        plugin.reset_password(
                                            &id,
                                            &password,
                                            password_reset_on_login,
                                        )?;
                                    }
                                } else if let Some(plugin) = datastore_plugins
                                    .iter()
                                    .find(|p| p.0.plugin_name == datastore)
                                {
                                    plugin.set_user(&id, &account)?;
                                }
                            }
                        }
                        AccountChange::RemoveAccount { .. } => {}
                    }

                    Ok(())
                };

                let mut error_func = |e: Box<dyn Error>, change: &AccountChange| {
                    error!("{}", e);
                    let (id, datastore) = match change {
                        AccountChange::AddAccount {
                            datastore, temp_id, ..
                        } => (temp_id.clone(), datastore.clone()),
                        AccountChange::RemoveAccount { datastore, id } => {
                            (id.clone(), datastore.clone())
                        }
                        AccountChange::EditAccount { id, datastore, .. } => {
                            (id.clone(), datastore.clone())
                        }
                    };
                    error_strings
                        .entry(id)
                        .or_insert_with(HashMap::new)
                        .entry(datastore)
                        .or_insert_with(HashSet::new)
                        .insert(e.to_string());
                };

                let ret = f();

                // Just in case there is a weird timeout thing we wait one minute on error
                if let Err(e) = ret {
                    error_func(e, &change);
                    spin_sleep::sleep(Duration::from_secs(60));

                    if let Err(e) = f() {
                        error_func(e, &change);
                    }
                }
            }

            if let Some(plugin) = datastore_plugins
                .iter()
                .find(|p| p.0.plugin_name == "google_admin_datastore")
            {
                plugin.0.run("clear_cache", ())?;
            }

            // TODO: Strange "duplicate user" errors from google_admin_datastore when doing changes
            //  on the real user data
            info!("Finished user commit");

            Ok(serialize(&error_strings))
        }
        _ => unimplemented!(),
    }
}

#[export_plugin_func("get")]
pub fn get(_page: &str) -> Result<Vec<u8>, Box<dyn Error>> {
    unimplemented!()
}
