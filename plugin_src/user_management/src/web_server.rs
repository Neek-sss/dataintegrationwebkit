// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use crate::shared::api::{AVAILABLE_DATASTORES, EDIT_ROW, OU_TREE, USER_LIST};
use datastore_core::shared::{
    datastore_core_plugin::DatastoreCorePlugin, datastore_plugin::DatastorePlugin,
};
use log::info;
use plugin_std::data::{AccountChange, AccountField, OrganizationalUnitTree, SourcedAccountEntry};
use plugin_std::macros::export_plugin_func;
use plugin_std::mem::{deserialize, serialize};
use std::{collections::HashMap, error::Error};
use web_program_core::shared::request::RowEditRequest;

#[export_plugin_func("init")]
pub fn init() {
    // Stuff
}

#[export_plugin_func("view")]
pub fn view(page: String) -> Result<(String, HashMap<String, String>), Box<dyn Error>> {
    match page.as_str() {
        "" | "index" => {
            let mut context: HashMap<String, String> = HashMap::new();
            context.insert("file_name".to_string(), "user_management".to_string());
            Ok(("wasm".to_string(), context))
        }
        _ => unimplemented!(),
    }
}

#[export_plugin_func("post")]
pub fn post((page, bytes): (String, Vec<u8>)) -> Result<Vec<u8>, Box<dyn Error>> {
    match page.as_str() {
        EDIT_ROW => Ok(serialize(&edit_row(&bytes)?)),
        _ => unimplemented!(),
    }
}

fn all_ou_trees() -> OrganizationalUnitTree {
    let mut ou_tree = OrganizationalUnitTree::default();

    let datastore_plugins = DatastorePlugin::all();
    for plugin in datastore_plugins {
        info!("Get tree for {}", plugin.0.plugin_name);
        if let Ok(ret_ou_tree) = plugin.ou_tree() {
            info!("Merging tree");
            ou_tree.merge_tree(&ret_ou_tree);
            info!("Finished merging tree");
        }
    }

    ou_tree
}

fn all_users_in_ou(
    ou: &OrganizationalUnitTree,
) -> Result<HashMap<String, HashMap<Vec<u8>, SourcedAccountEntry>>, Box<dyn Error>> {
    let mut ret = HashMap::new();
    ret.insert(
        ou.organization.clone(),
        managed_user_list(&ou.organization)?,
    );
    for child in &ou.children {
        for (child_ou, users) in all_users_in_ou(child)? {
            ret.insert(child_ou, users);
        }
    }
    Ok(ret)
}

fn all_available_datastores() -> Vec<String> {
    DatastorePlugin::all()
        .into_iter()
        .filter_map(|datastore| {
            let function_kinds = &datastore.0.function_kinds;
            if function_kinds.contains("ou_tree") && function_kinds.contains("get_data_list") {
                Some(datastore.0.plugin_name.replace("_datastore", ""))
            } else {
                None
            }
        })
        .collect()
}

#[export_plugin_func("get")]
pub fn get(page: String) -> Result<Vec<u8>, Box<dyn Error>> {
    match page.as_str() {
        AVAILABLE_DATASTORES => Ok(serialize(&all_available_datastores())),
        OU_TREE => Ok(serialize(&all_ou_trees())),
        USER_LIST => {
            let ou_tree = all_ou_trees();
            let list = all_users_in_ou(&ou_tree)?.into_iter().collect::<Vec<_>>();
            Ok(serialize(&list))
        }
        _ => unimplemented!(),
    }
}

fn edit_row(edit_bytes: &[u8]) -> Result<(), Box<dyn Error>> {
    let edits = deserialize::<RowEditRequest>(edit_bytes);

    let default_datastore_plugin = DatastoreCorePlugin::new().get_default_datastore()?;
    let datastore_plugins = DatastorePlugin::all();

    for edit in edits {
        info!("Edit {:?} received", edit);

        match edit {
            AccountChange::EditAccount { id, change, .. } => {
                let mut user = default_datastore_plugin.get_user(&id)?.unwrap_or({
                    let mut stored_user_opt = None;
                    for plugin in &datastore_plugins {
                        let stored_user = plugin.get_user(&id)?;
                        if stored_user.is_some() {
                            stored_user_opt = stored_user;
                            break;
                        }
                    }
                    if let Some(ad_user) = stored_user_opt {
                        default_datastore_plugin.set_user(&id, &ad_user)?
                    } else {
                        info!("No user for: {:?}", id);
                    }

                    default_datastore_plugin
                        .get_user(&id)?
                        .expect("Cannot get user")
                });

                info!("'Old' Entry: {:?}", user);

                match change {
                    AccountField::ID(_value) => {
                        // TODO: Actually make this do something
                    }
                    AccountField::FirstName(value) => user.first_name = value,
                    AccountField::LastName(value) => user.last_name = value,
                    AccountField::UserName(value) => user.user_name = value,
                    AccountField::Email(value) => user.email = value,
                    AccountField::Organization(value) => user.organization = value,
                    AccountField::Password(_, _)
                    | AccountField::Groups(_)
                    | AccountField::Enabled(_) => (),
                }

                info!("'New' entry: {:?}", user);

                default_datastore_plugin.set_user(&id, &user)?
            }
            AccountChange::AddAccount {
                temp_id,
                datastore,
                account,
                ..
            } => {
                info!(
                    "Adding {} {} to {}",
                    account.first_name, account.last_name, datastore
                );
                let mut sourced_user = SourcedAccountEntry::new();

                for plugin in &datastore_plugins {
                    if let Some(user) = plugin.get_user(&temp_id)? {
                        sourced_user.insert_unmanaged_user(
                            &user,
                            &plugin.0.plugin_name.replace("_datastore", ""),
                        );
                    }
                }

                if let Some(managed_user) = default_datastore_plugin.get_user(&temp_id)? {
                    sourced_user.insert_unmanaged_user(
                        &managed_user,
                        &default_datastore_plugin
                            .0
                            .plugin_name
                            .replace("_datastore", ""),
                    );
                }

                let existing_user = sourced_user.to_user();
                for plugin in &datastore_plugins {
                    if datastore == plugin.0.plugin_name {
                        plugin.add_user(&existing_user)?;
                    }
                }
            }
            AccountChange::RemoveAccount { id, datastore } => {
                info!("Remove account for {:?} from {}", id, datastore);
                for plugin in &datastore_plugins {
                    if datastore == plugin.0.plugin_name {
                        plugin.remove_user(&id)?;
                    }
                }
            }
        }
    }

    Ok(())
}

/// Gets the list of users from the sources
fn managed_user_list(
    organization: &str,
) -> Result<HashMap<Vec<u8>, SourcedAccountEntry>, Box<dyn Error>> {
    let mut ret: HashMap<Vec<u8>, SourcedAccountEntry> = HashMap::new();

    let datastore_plugins = DatastorePlugin::all();
    for plugin in &datastore_plugins {
        info!("Get user list for {}", plugin.0.plugin_name);
        if let Ok(account_list) = plugin.user_list(organization) {
            info!("Got user list from {}", plugin.0.plugin_name);
            for (id, account) in account_list {
                if let Some(user) = ret.values_mut().find(|u| {
                    (u.last_name
                        .current_value()
                        .unwrap_or_default()
                        .1
                        .to_ascii_lowercase()
                        == account.last_name.to_ascii_lowercase()
                        && u.first_name
                            .current_value()
                            .unwrap_or_default()
                            .1
                            .to_ascii_lowercase()
                            == account.first_name.to_ascii_lowercase())
                        || u.email
                            .current_value()
                            .unwrap_or_default()
                            .1
                            .to_ascii_lowercase()
                            .trim()
                            == account.email.to_ascii_lowercase().trim()
                }) {
                    user.insert_unmanaged_user(
                        &account,
                        &plugin.0.plugin_name.replace("_datastore", ""),
                    );
                } else {
                    let user = SourcedAccountEntry::from_unmanaged_user(
                        &account,
                        &plugin.0.plugin_name.replace("_datastore", ""),
                    );

                    ret.insert(id, user);
                }
            }
        }
    }

    let default_datastore_plugin = DatastoreCorePlugin::new().get_default_datastore()?;
    let database_list = default_datastore_plugin.user_list(organization)?;

    for (id, database_user) in database_list.into_iter() {
        if let Some(user) = ret.values_mut().find(|u| {
            (u.last_name
                .current_value()
                .unwrap_or_default()
                .1
                .to_ascii_lowercase()
                == database_user.last_name.to_ascii_lowercase()
                && u.first_name
                    .current_value()
                    .unwrap_or_default()
                    .1
                    .to_ascii_lowercase()
                    == database_user.first_name.to_ascii_lowercase())
                || u.email
                    .current_value()
                    .unwrap_or_default()
                    .1
                    .to_ascii_lowercase()
                    .trim()
                    == database_user.email.to_ascii_lowercase().trim()
        }) {
            user.set_managed_user(
                &database_user,
                &default_datastore_plugin
                    .0
                    .plugin_name
                    .replace("_datastore", ""),
            );
        } else {
            let account = SourcedAccountEntry::from_managed_user(
                &database_user,
                &default_datastore_plugin
                    .0
                    .plugin_name
                    .replace("_datastore", ""),
            );
            ret.insert(id, account);
        }
    }

    Ok(ret)
}
