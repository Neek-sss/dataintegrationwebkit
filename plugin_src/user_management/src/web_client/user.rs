// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)



use plugin_std::data::{SourcedAccountEntry, SourcedString};
use std::{cell::RefCell, collections::HashSet, rc::Rc};

#[derive(Clone, Debug, Default, PartialEq)]
pub struct User {
    pub id: Rc<RefCell<String>>,
    pub first_name: Rc<RefCell<SourcedString>>,
    pub last_name: Rc<RefCell<SourcedString>>,
    pub user_name: Rc<RefCell<SourcedString>>,
    pub email: Rc<RefCell<SourcedString>>,
    pub sled: Rc<RefCell<bool>>,
    pub active_directory: Rc<RefCell<bool>>,
    pub google: Rc<RefCell<bool>>,
}

impl From<SourcedAccountEntry> for User {
    fn from(entry: SourcedAccountEntry) -> Self {
        Self {
            id: Rc::new(RefCell::new(entry.id)),
            first_name: Rc::new(RefCell::new(entry.first_name)),
            last_name: Rc::new(RefCell::new(entry.last_name)),
            user_name: Rc::new(RefCell::new(entry.user_name)),
            email: Rc::new(RefCell::new(entry.email)),
            sled: Rc::new(RefCell::new(entry.datasource.contains("sled"))),
            active_directory: Rc::new(RefCell::new(
                entry.datasource.contains("active_directory_datastore"),
            )),
            google: Rc::new(RefCell::new(entry.datasource.contains("google"))),
        }
    }
}

impl From<User> for SourcedAccountEntry {
    fn from(user: User) -> Self {
        let mut datasource = HashSet::new();
        if *user.sled.borrow() {
            datasource.insert("sled".to_string());
        }
        if *user.active_directory.borrow() {
            datasource.insert("active_directory_datastore".to_string());
        }
        if *user.google.borrow() {
            datasource.insert("google".to_string());
        }
        Self {
            id: user.id.borrow().clone(),
            first_name: user.first_name.borrow().clone(),
            last_name: user.last_name.borrow().clone(),
            user_name: user.user_name.borrow().clone(),
            email: user.email.borrow().clone(),
            organization: Default::default(),
            datasource,
        }
    }
}
