// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use crate::web_client::component::management::UserManagementContext;
use log::debug;
use plugin_std::data::OrganizationalUnitTree;
use web_sys::HtmlElement;
use yew::{html, Callback, Component, Context, Html, NodeRef, Properties};

use web_program_core::shared::web_client::component::server_data_provider::ServerDataContext;
use yew::context::ContextHandle;

#[derive(Clone, Debug, Properties)]
/// The container for the page to display [OrganizationalUnitTrees](OrganizationalUnitTree) properly
pub struct OrganizationalUnitListProps {
    /// The tree to be displayed
    #[prop_or_default]
    pub(crate) ou_path: String,
    /// What to do when the organization is clicked
    #[prop_or_else(Callback::noop)]
    pub(crate) onclick: Callback<()>,
}

impl PartialEq for OrganizationalUnitListProps {
    fn eq(&self, other: &Self) -> bool {
        self.ou_path.eq(&other.ou_path)
    }
}

/// A collection of organization displayers
pub struct OrganizationalUnitList {
    /// Whether or not to display the children of the current organization
    pub(crate) children_hidden: bool,
    /// The reference to this particular object on the page
    list_node_ref: NodeRef,
    ou_tree_context: ServerDataContext<OrganizationalUnitTree, OrganizationalUnitTree>,
    _ou_tree_handle:
        ContextHandle<ServerDataContext<OrganizationalUnitTree, OrganizationalUnitTree>>,
    user_management_context: UserManagementContext,
    _user_management_handle: ContextHandle<UserManagementContext>,
}

/// Used to process interactions with the list
pub enum Message {
    /// The object has been clicked on the page
    Select(String),
    /// The children drop down has been clicked
    OpenChildren,
    OUTreeContext,
    UserManagementContext,
}

impl Component for OrganizationalUnitList {
    type Message = Message;
    type Properties = OrganizationalUnitListProps;

    fn create(ctx: &Context<Self>) -> Self {
        let (ou_tree_context, ou_tree_handle) = ctx
            .link()
            .context::<ServerDataContext<OrganizationalUnitTree, OrganizationalUnitTree>>(
                ctx.link().callback(|_| Message::OUTreeContext),
            )
            .expect("No OU Tree Context found");
        let (user_management_context, user_management_handle) = ctx
            .link()
            .context::<UserManagementContext>(
                ctx.link().callback(|_| Message::UserManagementContext),
            )
            .expect("No User Management Context found");

        Self {
            children_hidden: true,
            list_node_ref: Default::default(),
            ou_tree_context,
            _ou_tree_handle: ou_tree_handle,
            user_management_context,
            _user_management_handle: user_management_handle,
        }
    }

    fn update(&mut self, ctx: &Context<Self>, msg: Self::Message) -> bool {
        match msg {
            Message::Select(ou) => {
                debug!("Selected OU: {}", ou);
                self.user_management_context.current_ou = ou.clone();
                self.user_management_context.update_current_ou_func.emit(ou);
            }
            Message::OpenChildren => {
                debug!("Opened children of OU: {}", ctx.props().ou_path);

                let ou_tree = self
                    .ou_tree_context
                    .data
                    .borrow()
                    .find_sub_tree(&self.user_management_context.current_ou)
                    .unwrap();
                if ou_tree.children.is_empty() {
                    let list_node_element = self.list_node_ref.cast::<HtmlElement>().unwrap();

                    if self.children_hidden {
                        list_node_element
                            .remove_attribute("hidden")
                            .expect("Cannot show managed p");
                    } else {
                        list_node_element
                            .set_attribute("hidden", "true")
                            .expect("Cannot show managed p");
                    }
                    self.children_hidden = !self.children_hidden;
                }
            }
            Message::OUTreeContext => {
                let (ou_tree_context, _) = ctx
                    .link()
                    .context::<ServerDataContext<OrganizationalUnitTree, OrganizationalUnitTree>>(
                        ctx.link().callback(|_| Message::OUTreeContext),
                    )
                    .expect("No OU Tree Context found");
                self.ou_tree_context = ou_tree_context;
            }
            Message::UserManagementContext => {
                let (user_management_context, _) = ctx
                    .link()
                    .context::<UserManagementContext>(
                        ctx.link().callback(|_| Message::UserManagementContext),
                    )
                    .expect("No User Management Context found");
                self.user_management_context = user_management_context;
            }
        }
        true
    }

    fn changed(&mut self, _ctx: &Context<Self>, _props: &Self::Properties) -> bool {
        true
    }

    fn view(&self, ctx: &Context<Self>) -> Html {
        let ou_name = ctx
            .props()
            .ou_path
            .rsplit('/')
            .next()
            .unwrap()
            .replace('\\', "");

        let (ou_tree, selected) =
            if ou_name.is_empty() && self.user_management_context.current_ou.is_empty() {
                (self.ou_tree_context.data.borrow().clone(), true)
            } else {
                let ou_tree = self
                    .ou_tree_context
                    .data
                    .borrow()
                    .find_sub_tree(&ctx.props().ou_path)
                    .unwrap_or_default();
                let selected = self.user_management_context.current_ou == ctx.props().ou_path;
                (ou_tree, selected)
            };

        let ou_path_1 = ctx.props().ou_path.clone();
        let ou_path_2 = ou_path_1.clone();
        let name_html = if selected {
            html! {
                <p style="display: inline-block;" class="hover" onclick={ctx.link().callback(move |_| Message::Select(ou_path_1.clone()))}>
                    <b>
                        { ou_name }
                    </b>
                </p>
            }
        } else {
            html! {
                <p style="display: inline-block;" class="hover" onclick={ctx.link().callback(move |_| Message::Select(ou_path_2.clone()))}>
                    { ou_name }
                </p>
            }
        };

        let ret = if ou_tree.children.is_empty() {
            html! {
                <div>
                    {
                        name_html
                    }
                </div>
            }
        } else {
            let hidden = !self
                .user_management_context
                .current_ou
                .contains(&ou_tree.organization);
            html! {
                <div>
                    <div style="white-space: nowrap;">
                        <div style="display: inline-block;" class="hover" onclick={ctx.link().callback(|_| Message::OpenChildren)}>
                            { "[↳]" }
                        </div>
                        {
                            name_html
                        }
                    </div>
                    <ul ref={self.list_node_ref.clone()} hidden={hidden}>
                        {
                            ou_tree
                                .children
                                .iter()
                                .map(|c| {
                                    let props = OrganizationalUnitListProps {
                                        ou_path: c.organization.clone(),
                                        onclick: ctx.props().onclick.clone(),
                                    };
                                    html! { <li><OrganizationalUnitList ..props/></li> }
                                })
                                .collect::<Html>()
                        }
                    </ul>
                </div>
            }
        };
        ret
    }
}
