// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use core::cell::RefCell;
use std::collections::HashMap;

use std::rc::Rc;
use yew::context::ContextHandle;
use yew::{html, prelude::*};

// use crate::web_client::component::account_row::message::Message;
use plugin_std::data::SourcedAccountEntry;
use plugin_std::mem::serialize;
use web_program_core::shared::web_client::component::{
    server_data_provider::ServerDataContext,
    spreadsheet::{
        cell::Cell, component::RowProps, display::uneditable_string::UneditableStringDisplay,
        row::Row,
    },
};

// pub mod message;

#[derive(Debug)]
/// A single row in the user management sheet
pub struct AccountRow {
    data_context: ServerDataContext<
        HashMap<String, Vec<(Vec<u8>, SourcedAccountEntry)>>,
        (Vec<u8>, SourcedAccountEntry),
    >,
    _data_handle: ContextHandle<
        ServerDataContext<
            HashMap<String, Vec<(Vec<u8>, SourcedAccountEntry)>>,
            (Vec<u8>, SourcedAccountEntry),
        >,
    >,
    datastores_context: ServerDataContext<Vec<String>, String>,
    _datastores_handle: ContextHandle<ServerDataContext<Vec<String>, String>>,
}

impl Row for AccountRow {
    fn header_strings() -> Vec<String> {
        vec![
            "First Name".to_string(),
            "Last Name".to_string(),
            "User Name".to_string(),
            "Email".to_string(),
            "Datastores".to_string(),
        ]
    }
}

pub enum Message {
    DataContext,
    DatastoresContext,
}

impl Component for AccountRow {
    type Message = Message;
    type Properties = RowProps<(Vec<u8>, SourcedAccountEntry)>;

    fn create(ctx: &Context<Self>) -> Self {
        let (data_context, data_handle) = ctx
            .link()
            .context::<ServerDataContext<
                HashMap<String, Vec<(Vec<u8>, SourcedAccountEntry)>>,
                (Vec<u8>, SourcedAccountEntry),
            >>(ctx.link().callback(|_| Message::DataContext))
            .expect("No Data Context found");
        let (datastores_context, datastores_handle) = ctx
            .link()
            .context::<ServerDataContext<Vec<String>, String>>(
                ctx.link().callback(|_| Message::DatastoresContext),
            )
            .expect("No Datastores Context found");

        Self {
            data_context,
            _data_handle: data_handle,
            datastores_context,
            _datastores_handle: datastores_handle,
        }
    }

    fn update(&mut self, ctx: &Context<Self>, msg: Self::Message) -> bool {
        match msg {
            Message::DataContext => {
                let (data_context, _) = ctx
                    .link()
                    .context::<ServerDataContext<
                        HashMap<String, Vec<(Vec<u8>, SourcedAccountEntry)>>,
                        (Vec<u8>, SourcedAccountEntry),
                    >>(ctx.link().callback(|_| Message::DataContext))
                    .expect("No Data Context found");
                self.data_context = data_context;
            }
            Message::DatastoresContext => {
                let (datastores_context, _) = ctx
                    .link()
                    .context::<ServerDataContext<Vec<String>, String>>(
                        ctx.link().callback(|_| Message::DatastoresContext),
                    )
                    .expect("No Datastores Context found");
                self.datastores_context = datastores_context;
            }
        }
        true
    }

    fn view(&self, ctx: &Context<Self>) -> Html {
        let props = ctx.props();
        let id = Rc::new(RefCell::new(serialize(&props.data.0)));
        let first_name_cell = Cell::new(
            id.clone(),
            props.row_index,
            0,
            &props.data.1.first_name.to_string(),
            None,
        );
        let last_name_cell = Cell::new(
            id.clone(),
            props.row_index,
            1,
            &props.data.1.last_name.to_string(),
            None,
        );
        let user_name_cell = Cell::new(
            id.clone(),
            props.row_index,
            2,
            &props.data.1.user_name.to_string(),
            None,
        );
        let email_cell = Cell::new(
            id.clone(),
            props.row_index,
            3,
            &props.data.1.email.to_string(),
            None,
        );
        let mut datastores = String::new();
        for datastore in self.datastores_context.data.borrow().iter() {
            if props.data.1.datasource.contains(datastore) {
                datastores += datastore;
                datastores += ",";
            }
        }
        if !datastores.is_empty() {
            datastores.pop();
        }
        let datastores_cell = Cell::new(id.clone(), props.row_index, 4, &datastores, None);
        html! {
            <tr>
                <UneditableStringDisplay ..first_name_cell/>
                <UneditableStringDisplay ..last_name_cell/>
                <UneditableStringDisplay ..user_name_cell/>
                <UneditableStringDisplay ..email_cell/>
                <UneditableStringDisplay ..datastores_cell/>
            </tr>
        }
        // let props = ctx.props();
        // let account = props.data.borrow();
        // let id = Rc::new(RefCell::new(account.id.clone()));
        // let first_name_cell = Cell::new(
        //     id.clone(),
        //     props.row_index,
        //     0,
        //     &account.first_name,
        //     props.version.clone(),
        //     props.edits.clone(),
        // );
        // let last_name_cell = Cell::new(
        //     id.clone(),
        //     props.row_index,
        //     1,
        //     &account.last_name,
        //     props.version.clone(),
        //     props.edits.clone(),
        // );
        // let user_name_cell = Cell::new(
        //     id.clone(),
        //     props.row_index,
        //     2,
        //     &account.user_name,
        //     props.version.clone(),
        //     props.edits.clone(),
        // );
        // let user_name_props = EditableSourcedStringProps {
        //     cell: user_name_cell,
        //     on_edit: ctx
        //         .link()
        //         .callback(|_| Message::StringEdit("user_name".to_string())),
        // };
        // let email_cell = Cell::new(
        //     id.clone(),
        //     props.row_index,
        //     3,
        //     &account.email,
        //     props.version.clone(),
        //     props.edits.clone(),
        // );
        // let email_props = EditableSourcedStringProps {
        //     cell: email_cell,
        //     on_edit: ctx
        //         .link()
        //         .callback(|_| Message::StringEdit("email".to_string())),
        // };
        // let sled_cell = Cell::new(
        //     id.clone(),
        //     props.row_index,
        //     4,
        //     &props.data.borrow().datasource.contains("sled"),
        //     props.version.clone(),
        //     props.edits.clone(),
        // );
        // let sled_props = CheckboxSourcedProps {
        //     cell: sled_cell,
        //     on_edit: ctx.link().callback(|_| Message::SledCheckboxEdit),
        // };
        // let active_directory_cell = Cell::new(
        //     id.clone(),
        //     props.row_index,
        //     5,
        //     &props
        //         .data
        //         .borrow()
        //         .datasource
        //         .contains("active_directory")
        //         .clone(),
        //     props.version.clone(),
        //     props.edits.clone(),
        // );
        // let active_directory_props = CheckboxSourcedProps {
        //     cell: active_directory_cell,
        //     on_edit: ctx
        //         .link()
        //         .callback(|_| Message::ActiveDirectoryCheckboxEdit),
        // };
        // let google_cell = Cell::new(
        //     id.clone(),
        //     props.row_index,
        //     6,
        //     &props.data.borrow().datasource.contains("google").clone(),
        //     props.version.clone(),
        //     props.edits.clone(),
        // );
        // let google_props = CheckboxSourcedProps {
        //     cell: google_cell,
        //     on_edit: ctx.link().callback(|_| Message::GoogleCheckboxEdit),
        // };
        // let button_props = ButtonProps {
        //     label: "Generate Password".to_string(),
        //     on_click: ctx.link().callback(|_| Message::GenerateResetPasswordLink),
        // };
        // let ret = if let Some(link) = &self.password_reset_link {
        //     html! {
        //         <tr>
        //             <UneditableSourcedStringDisplay<SourcedAccountEntry> ..first_name_cell/>
        //             <UneditableSourcedStringDisplay<SourcedAccountEntry> ..last_name_cell/>
        //             <EditableSourcedStringDisplay<SourcedAccountEntry> ..user_name_props/>
        //             <EditableSourcedStringDisplay<SourcedAccountEntry> ..email_props/>
        //             <CheckboxSourcedDisplay<SourcedAccountEntry> ..sled_props/>
        //             <CheckboxSourcedDisplay<SourcedAccountEntry> ..active_directory_props/>
        //             <CheckboxSourcedDisplay<SourcedAccountEntry> ..google_props/>
        //             <td>
        //                 <a href={link.clone()} target="_blank" rel="noopener noreferrer">{ "link" }</a>
        //             </td>
        //         </tr>
        //     }
        // } else {
        //     html! {
        //         <tr>
        //             <UneditableSourcedStringDisplay<SourcedAccountEntry> ..first_name_cell/>
        //             <UneditableSourcedStringDisplay<SourcedAccountEntry> ..last_name_cell/>
        //             <EditableSourcedStringDisplay<SourcedAccountEntry> ..user_name_props/>
        //             <EditableSourcedStringDisplay<SourcedAccountEntry> ..email_props/>
        //             <CheckboxSourcedDisplay<SourcedAccountEntry> ..sled_props/>
        //             <CheckboxSourcedDisplay<SourcedAccountEntry> ..active_directory_props/>
        //             <CheckboxSourcedDisplay<SourcedAccountEntry> ..google_props/>
        //             <ButtonDisplay ..button_props/>
        //         </tr>
        //     }
        // };
    }
}
