// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)



use crate::web_client::component::account_row::AccountRow;
use log::debug;
use reset_password::shared::api::VIEW_RESET_PASSWORD_LINK;
use web_program_core::ROOT_URL;

/// Got a password reset link from the server
pub fn update(form: &mut AccountRow, data: String) -> bool {
    debug!(
        "Password redirect link: {}{}{}",
        ROOT_URL, VIEW_RESET_PASSWORD_LINK, data
    );

    form.password_reset_link = Some(format!("{}{}{}", ROOT_URL, VIEW_RESET_PASSWORD_LINK, data));

    true
}
