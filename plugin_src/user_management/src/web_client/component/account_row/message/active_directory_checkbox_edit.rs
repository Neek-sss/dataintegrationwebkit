// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)



use crate::web_client::component::account_row::AccountRow;
use log::debug;
use plugin_std::data::Change;
use yew::Context;

/// Enable/Disable the Active Directory account
pub fn update(ctx: &Context<AccountRow>) -> bool {
    debug!("Active Directory checkbox edit");
    let props = ctx.props();
    let mut data = props.data.borrow_mut();
    let ad = data.datasource.contains("active_directory");
    if ad {
        data.datasource.remove("active_directory");
        data.first_name.remove_unmanaged_string("active_directory");
        data.last_name.remove_unmanaged_string("active_directory");
        data.user_name.remove_unmanaged_string("active_directory");
        data.email.remove_unmanaged_string("active_directory");
        data.organization
            .remove_unmanaged_string("active_directory");
        props.edits.borrow_mut().push(Change::Remove {
            id: data.id.clone(),
            data: data.clone(),
        });
        true
    } else {
        data.datasource.insert("active_directory".to_string());
        if let Some((_, first_name)) = data.first_name.current_value() {
            data.first_name
                .add_unmanaged_string(&first_name, "active_directory");
        }
        if let Some((_, last_name)) = data.last_name.current_value() {
            data.last_name
                .add_unmanaged_string(&last_name, "active_directory");
        }
        if let Some((_, user_name)) = data.user_name.current_value() {
            data.user_name
                .add_unmanaged_string(&user_name, "active_directory");
        }
        if let Some((_, email)) = data.email.current_value() {
            data.email.add_unmanaged_string(&email, "active_directory");
        }
        if let Some((_, organization)) = data.organization.current_value() {
            data.organization
                .add_unmanaged_string(&organization, "active_directory");
        }
        props.edits.borrow_mut().push(Change::Add {
            id: data.id.clone(),
            data: data.clone(), // TODO: Passwords
        });
        true
    }
}
