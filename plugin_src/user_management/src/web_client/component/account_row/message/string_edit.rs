// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)



use crate::web_client::component::account_row::AccountRow;
use log::debug;
use yew::Context;

use plugin_std::data::Change;
use plugin_std::mem::serialize;

/// A string has been edited, and so the server should be updated
pub fn update(ctx: &Context<AccountRow>, column_name: String) -> bool {
    debug!("String edit");
    let props = ctx.props();
    let id = props.data.borrow().id.clone();
    let serialized_data = match column_name.as_str() {
        "user_name" => serialize(&props.data.borrow().user_name),
        "email" => serialize(&props.data.borrow().email),
        _ => unreachable!(),
    };

    props.edits.borrow_mut().push(Change::Edit {
        id,
        field_id: column_name,
        field_data: serialized_data,
    });

    let managed = { props.data.borrow_mut().datasource.contains("sled") };
    if managed {
        false
    } else {
        props
            .data
            .borrow_mut()
            .datasource
            .insert("sled".to_string());
        true
    }
}
