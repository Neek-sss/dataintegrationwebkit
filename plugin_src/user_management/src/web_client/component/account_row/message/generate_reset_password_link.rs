// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use std::borrow::Borrow;

use crate::web_client::component::{account_row::AccountRow, account_row::Message};
use log::debug;
use reset_password::shared::api::GENERATE_RESET_PASSWORD_LINK;
use web_program_core::shared::web_client::request::{
    web_server_post, GenerateResetPasswordLinkRequest,
};
use yew::Context;

/// Generate a password reset link from the server
pub fn update(ctx: &Context<AccountRow>) -> bool {
    let props = ctx.props();
    debug!(
        "Generate Reset Password Link for {}",
        props
            .data
            .borrow()
            .user_name
            .current_value()
            .unwrap_or_else(Default::default)
            .1
    );

    let user: GenerateResetPasswordLinkRequest = props
        .data
        .borrow()
        .id
        .current_value()
        .unwrap_or_else(Default::default)
        .1;

    let link_1 = ctx.link().clone();
    let link_2 = ctx.link().clone();
    web_server_post(
        &user,
        GENERATE_RESET_PASSWORD_LINK,
        Box::new(move |data| {
            if let Some(data) = data {
                link_1.send_message(Message::Fetch(data))
            } else {
                link_1.send_message(Message::Error)
            }
        }),
        Box::new(move |_| link_2.send_message(Message::Error)),
    );

    false
}
