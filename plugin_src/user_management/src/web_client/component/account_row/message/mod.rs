// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

pub mod active_directory_checkbox_edit;
pub mod error;
pub mod fetch;
pub mod generate_reset_password_link;
pub mod google_checkbox_edit;
pub mod sled_checkbox_edit;
pub mod string_edit;

use crate::web_client::component::account_row::AccountRow;

use yew::Context;

/// The struct for handling user and server interactions
pub enum Message {
    /// One of the strings has been edited
    StringEdit(String),
    /// The Sled checkbox has been clicked
    SledCheckboxEdit,
    /// The Active Directory checkbox has been clicked
    ActiveDirectoryCheckboxEdit,
    /// The Google checkbox has been clicked
    GoogleCheckboxEdit,
    /// A request for generating a new password has been clicked
    GenerateResetPasswordLink,
    /// Got data from the server
    Fetch(String),
    /// There was an error with the server connection
    Error,
}

impl Message {
    /// Process the message data for each message type
    pub fn update(self, form: &mut AccountRow, ctx: &Context<AccountRow>) -> bool {
        match self {
            Message::StringEdit(column_name) => string_edit::update(ctx, column_name),
            Message::SledCheckboxEdit => sled_checkbox_edit::update(ctx),
            Message::ActiveDirectoryCheckboxEdit => active_directory_checkbox_edit::update(ctx),
            Message::GoogleCheckboxEdit => google_checkbox_edit::update(ctx),
            Message::GenerateResetPasswordLink => generate_reset_password_link::update(ctx),
            Message::Fetch(data) => fetch::update(form, data),
            Message::Error => error::update(),
        }
    }
}
