// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)



use crate::web_client::component::account_row::AccountRow;
use log::debug;
use plugin_std::data::Change;
use yew::Context;

/// Enable/Disable the Google account
pub fn update(ctx: &Context<AccountRow>) -> bool {
    debug!("Google checkbox edit");
    let props = ctx.props();
    let mut data = props.data.borrow_mut();
    let google = data.datasource.contains("google");
    if google {
        data.datasource.remove("google");
        data.first_name.remove_unmanaged_string("google");
        data.last_name.remove_unmanaged_string("google");
        data.user_name.remove_unmanaged_string("google");
        data.email.remove_unmanaged_string("google");
        data.organization.remove_unmanaged_string("google");
        props.edits.borrow_mut().push(Change::Remove {
            id: data.id.clone(),
            data: data.clone(),
        });
        true
    } else {
        data.datasource.insert("google".to_string());
        if let Some((_, first_name)) = data.first_name.current_value() {
            data.first_name.add_unmanaged_string(&first_name, "google");
        }
        if let Some((_, last_name)) = data.last_name.current_value() {
            data.last_name.add_unmanaged_string(&last_name, "google");
        }
        if let Some((_, user_name)) = data.user_name.current_value() {
            data.user_name.add_unmanaged_string(&user_name, "google");
        }
        if let Some((_, email)) = data.email.current_value() {
            data.email.add_unmanaged_string(&email, "google");
        }
        if let Some((_, organization)) = data.organization.current_value() {
            data.organization
                .add_unmanaged_string(&organization, "google");
        }
        props.edits.borrow_mut().push(Change::Add {
            id: data.id.clone(),
            data: data.clone(), // TODO: Passwords
        });
        true
    }
}
