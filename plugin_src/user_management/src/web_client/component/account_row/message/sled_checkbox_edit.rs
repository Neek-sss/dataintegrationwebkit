// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)



use crate::web_client::component::account_row::AccountRow;
use log::debug;
use plugin_std::data::Change;
use yew::Context;

/// Enable/Disable the Sled account
pub fn update(ctx: &Context<AccountRow>) -> bool {
    debug!("Sled checkbox edit");
    let props = ctx.props();
    let mut data = props.data.borrow_mut();
    let sled = data.datasource.contains("sled");
    if sled {
        data.datasource.remove("sled");
        data.first_name.remove_managed_string();
        data.last_name.remove_managed_string();
        data.user_name.remove_managed_string();
        data.email.remove_managed_string();
        data.organization.remove_managed_string();
        props.edits.borrow_mut().push(Change::Remove {
            id: data.id.clone(),
            data: data.clone(),
        });
        true
    } else {
        data.datasource.insert("sled".to_string());
        props.edits.borrow_mut().push(Change::Add {
            id: data.id.clone(),
            data: data.clone(), // TODO: Passwords
        });
        true
    }
}
