// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use crate::shared::api::{AVAILABLE_DATASTORES, EDIT_ROW, OU_TREE, USER_LIST};
use crate::web_client::component::{account_row::AccountRow, ou_list::OrganizationalUnitList};
use log::debug;
use plugin_std::data::{OrganizationalUnitTree, SourcedAccountEntry};
use std::collections::HashMap;
use web_program_core::shared::web_client::component::server_data_provider::ServerDataProvider;
use web_program_core::shared::web_client::component::sidebar::Sidebar;
use web_program_core::shared::web_client::component::spreadsheet::component::Spreadsheet;
use yew::{html, prelude::*};

#[derive(Clone, Debug, PartialEq)]
pub struct UserManagementContext {
    pub current_ou: String,
    pub update_current_ou_func: Callback<String>,
}

/// The object representing the overall User Management setup
pub struct UserManagementInner {
    current_ou: String,
}

/// Used to process the interactions with the client (both from the server and the user)
pub enum Message {
    /// The sidebar has been clicked by the user
    SidebarClick(String),
    // /// The server has responded with the data
    // Fetch(UserDataResponse),
    // /// There was an error getting the data from the server
    // Error,
}

impl Component for UserManagementInner {
    type Message = Message;
    type Properties = ();

    fn create(_ctx: &Context<Self>) -> Self {
        Self {
            current_ou: String::new(),
        }
    }

    fn update(&mut self, _ctx: &Context<Self>, msg: Self::Message) -> bool {
        match msg {
            Message::SidebarClick(ou) => {
                debug!("Sidebar was clicked");
                self.current_ou = ou;
            }
        }
        true
    }

    fn changed(&mut self, _ctx: &Context<Self>, _props: &Self::Properties) -> bool {
        true
    }

    fn view(&self, ctx: &Context<Self>) -> Html {
        let context = UserManagementContext {
            current_ou: self.current_ou.clone(),
            update_current_ou_func: ctx.link().callback(Message::SidebarClick),
        };

        let spreadsheet_style = r#"
                    margin-left: 250px;
                    padding: 1px 16px;
                    height: 1000px;
                "#;

        let ret = html! {
            <ContextProvider<UserManagementContext> context={context}>
                <Sidebar>
                    <OrganizationalUnitList/>
                </Sidebar>
                <div style={spreadsheet_style}>
                    <Spreadsheet<AccountRow, (Vec<u8>, SourcedAccountEntry), (Vec<u8>, SourcedAccountEntry)>
                        sheet_name={self.current_ou.clone()}
                    />
                </div>
            </ContextProvider<UserManagementContext>>
        };
        ret
    }
}

type IdSourcedAccountEntry = (Vec<u8>, SourcedAccountEntry);

#[function_component(UserManagement)]
pub fn user_management() -> Html {
    html! {
        <ServerDataProvider<Vec<String>, String> api_get_url={AVAILABLE_DATASTORES}>
            <ServerDataProvider<OrganizationalUnitTree, OrganizationalUnitTree> api_get_url={OU_TREE}>
                <ServerDataProvider<HashMap<String, Vec<IdSourcedAccountEntry>>, IdSourcedAccountEntry> api_get_url={USER_LIST} api_edit_url={EDIT_ROW}>
                    <UserManagementInner />
                </ServerDataProvider<HashMap<String, Vec<IdSourcedAccountEntry>>, IdSourcedAccountEntry>>
            </ServerDataProvider<OrganizationalUnitTree, OrganizationalUnitTree>>
        </ServerDataProvider<Vec<String>, String>>
    }
}
