// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

pub mod component;
// pub mod loading_state;
// pub mod user;

use component::management::UserManagement;
use wasm_bindgen::prelude::*;

#[wasm_bindgen(start)]
pub fn start() -> Result<(), JsValue> {
    #[cfg(feature = "console_error_panic_hook")]
    console_error_panic_hook::set_once();

    #[cfg(feature = "logging")]
    wasm_logger::init(wasm_logger::Config::new(log::Level::Trace));

    yew::Renderer::<UserManagement>::new().render();

    Ok(())
}
