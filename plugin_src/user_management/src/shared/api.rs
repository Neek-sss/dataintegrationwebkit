// "Whatever you do, work at it with all your heart, as working for the Lord,
// not for human masters, since you know that you will receive an inheritance
// from the Lord as a reward. It is the Lord Christ you are serving."
// (Col 3:23-24)

use web_program_core::apis;

apis! {
    "user_management",
    AVAILABLE_DATASTORES ("get") => "available_datastores",
    USER_LIST ("get") => "user_list",
    EDIT_ROW ("post") => "edit_row",
    OU_TREE ("get") => "ou_tree",
}
